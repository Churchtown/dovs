(* AU Compilation 2015.
 *
 * This the main working file for Part 4 - Semantic Analysis
 *
 *)



(* Our signature that for the semantic interface exposes only one function. *)

signature SEMANT =
sig
  val transProg: Absyn.exp -> TAbsyn.exp
end

structure Semant :> SEMANT =
struct


structure S = Symbol
structure A = Absyn
structure E = Env
structure Ty = Types
structure PT = PrintTypes
structure TAbs = TAbsyn

exception NotImplemented

(*--------------------------------------------------------*)
(* Use the extra record to add more information when traversing the tree
   It should become obvious when you actually need it, what to do.
   Alternatively, you have to add extra parameters to your functions *)

type extra = {inloop : bool ref, loopvar : Symbol.symbol option }

(* NilExp and ERRORty exp, to return when type checking fails*)

val errorExp = {exp = TAbs.NilExp, ty = Ty.ERROR}


(*--------------------------------------------------------*)
(* Error messages *)

val err = ErrorMsg.error

fun out msg pos = err pos msg

fun errorInt (pos, ty) =
    err pos ("INT required, " ^ PT.asString ty ^ " provided")

fun errorIntorString (pos, ty) =
    err pos ("INT or STRING required, " ^ PT.asString ty ^ " provided")

fun errorUnit (pos, ty) =
    err pos ("UNIT required, " ^ PT.asString ty ^ " provided")

fun errorNil (pos, id) =
    err pos ("need to give " ^ S.name id ^ " a type when assigning the value nil")

fun errorThenElse (pos, thenTy, elseTy) =
    err pos ("Then/Else type mismatch. Type of then: " ^ PT.asString thenTy ^ " Type of else: " ^ PT.asString elseTy )

fun errorWhileCheck (pos, checktype) =
    err pos ("Error in whileexpression. Type of the check should be int, found type: " ^ PT.asString checktype)

fun errorWhileBody (pos, bodytype) =
    err pos ("Error in whileexpression. Type of body should be unit, found type: " ^ PT.asString bodytype)

fun errorLoopBreak (pos) =
    err pos ("Error: break can not be outside a loop.")

fun errorForHi (pos, hitype) =
    err pos ("High type in for-loop has to be int, found type: " ^ PT.asString hitype)

fun errorForLo (pos, lotype) =
    err pos ("Low type in for-loop has to be int, found type: " ^ PT.asString lotype)

fun errorForBody (pos, bodytype) =
    err pos ("Body type in for-loop has to be unit, found type: " ^ PT.asString bodytype)

fun errorInitNil (pos) = 
    err pos ("Error: the init type can not be nil")

fun errorConstraintInit (pos, initType, consType) =
    err pos ("constraint and init are not the same type. Init is of type " ^ PT.asString initType ^ ", constraint is of type " ^ PT.asString consType)

fun errorOpSides (pos, lefttype, righttype) =
    err pos ("The two sides of an equation has to match types. Left type found: " ^ PT.asString lefttype ^ ", right type found: " ^ PT.asString righttype)

fun errorConstraintTypeNotFound (pos, constraintTy) =
    err pos ("type constraint " ^ S.name constraintTy ^ " not found in our type-environment")

fun errorVarNotFun (pos, varTy) = 
    err pos ("found a variable of type " ^ PT.asString varTy ^ ", instead of a function")

fun errorFunNotFound (pos, funName) = 
    err pos ("This function is not in the environment: " ^ funName)

fun errorFunArgs (pos, arrLength, formLength) = 
    err pos (" The number of types don't match. Nr of actual args: " ^ Int.toString(arrLength) ^ " Nr of formals: " ^ Int.toString(formLength))

fun errorFunTypeMismatch (pos) = 
    err pos ("Actual arg types and formal types don't match.")

fun errorTypeExistsAlready (pos, name) =
    err pos ("Type is already in environment: " ^ S.name name)

fun errorTypeNotInEnv (pos, sym)=
    err pos ("Type not found in environment " ^ S.name sym)

fun errorNameNoType (pos, s) = 
    err pos ("Name Type " ^ S.name s ^ " not pointing to any actual type")

fun errorAssignableTypes (pos, msg) = 
    err pos ("Type mismatch in " ^ msg)

fun errorCycleInDecs (pos) = 
    err pos ("There is a cycle in typedecs")

fun errorAssignTypeMismatch (pos, varty, expty) =
    err pos ("Assign types don't match. Type of variable: " ^ PT.asString varty ^ " Type of expression: " ^ PT.asString expty)

fun errorFunctionNotVar (pos, id) =
    err pos ("No var found: " ^ S.name id)

fun errorNotAnArray (pos, ty) =
    err pos ("The type " ^ PT.asString ty ^ " was found, instead of an array")
    
fun errorArray (pos) =
    err pos ("Var is not an array")

fun errorFunNotRecord (pos) =
    err pos ("var is not of type record")

fun errorReturnTypeMismatch(name, bodyExpTy, resultTy, pos) = 
    err pos (S.name name ^ " returns the type " ^ PT.asString bodyExpTy ^ " but should return " ^ PT.asString resultTy)

fun errorNameNotDeclared(pos, id) =
    err pos (S.name id ^ "is not declared")

fun errorAssignable(pos, msg) =
    err pos msg

fun errorVarNotArray(pos) =
    err pos ("var is not type array")

fun errorExpNotInt (pos) =
    err pos ("exp does not have type int")

fun errorNameAlreadyDeclared (pos, name) =
    err pos (S.name name ^ "is already declared")

fun duplicateNameInFunctionCall (pos, name) =
    err pos (S.name name ^ "duplicate parameter name in function")

fun errorRedundant (pos) =
    err pos ("What? This shouldn't happen.")

fun errorMayNotAssignToLoopVar (pos) =
    err pos ("Not allowed to assign to loop varible")

(*--------------------------------------------------------*)
(* -- Helper functions -- *)

(* Use the below three funs like:
   throw errorUnit (pos,ty) ifFalse isUnit(_) *)

fun ifTrue test err args = if test
                           then err args
                           else ()

fun ifFalse test err args = if test
                            then ()
                            else err args

fun throw err args testFun test  = testFun test err args

fun lookupTy tenv sym pos =
    let
        val tyOpt = S.look (tenv, sym)
    in
        case tyOpt of 
            SOME t => t 
          | NONE => (errorTypeNotInEnv(pos, sym) ; Ty.ERROR)
    end

fun actualTy (Ty.NAME (s, ty)) pos =
    (case !ty of
      NONE => (errorNameNoType(pos,s); Ty.ERROR)
    | SOME t => actualTy t pos)
    | actualTy t _ = t

fun compareTy(Ty.INT, Ty.INT, pos, tenv) = true
    | compareTy(Ty.STRING, Ty.STRING, _, _) = true
    | compareTy(Ty.RECORD(_,u1), Ty.RECORD(_,u2), _, _) = (u1 = u2)
    | compareTy(Ty.NIL,Ty.RECORD(_,_), _, _) = true
    | compareTy(Ty.RECORD(_,_),Ty.NIL, _, _) = true  
    | compareTy(Ty.ARRAY(_,u1),Ty.ARRAY(_,u2), _, _) = (u1 = u2)
    | compareTy(Ty.NIL,Ty.NIL, _, _) = true
    | compareTy(Ty.UNIT,_, _, _) = true
    | compareTy(_,Ty.UNIT, _, _) = true
    | compareTy(Ty.NAME(s,ty), Ty.NAME(s2,ty2), pos,tenv) = let
        val type1 = S.look(tenv, s)
        val type2 = S.look(tenv, s2)
        in
        (case (type1,type2) of
        (SOME(ty),SOME(ty2))=> compareTy((actualTy ty pos), (actualTy ty2 pos),pos, tenv)
        | (SOME(ty),NONE) => (errorNameNoType(pos, s); false)
        | (NONE,SOME(ty)) => (errorNameNoType(pos, s2); false)
        | (NONE,NONE) => (errorNameNoType(pos, s2); errorNameNoType(pos, s); false))
        end
    | compareTy(_,_,_, _) = false

fun checkInt (ty, pos) =
    case ty
     of Ty.INT => ()
      | Ty.ERROR => ()
      | _ => errorInt (pos, ty)

fun isUnit (ty) =
    case ty
     of Ty.UNIT => true
      | Ty.ERROR => true
      | _ => false

fun checkAssignable (declared: Ty.ty, assigned: Ty.ty, pos, msg) =
    let
        val aDeclared = actualTy declared pos
        val aAssigned = actualTy assigned pos
    in
        case (aDeclared, aAssigned) of
          (Ty.INT, Ty.INT)              => true
        | (Ty.STRING, Ty.STRING)        => true
        | (Ty.RECORD _, Ty.NIL)         => true
        | (Ty.NIL, Ty.RECORD _)         => true
        | (Ty.RECORD _, Ty.RECORD _)    => true
        | (Ty.ARRAY _, Ty.ARRAY _)      => true
        | (Ty.ARRAY (ty,_), Ty.INT)     => if ((actualTy ty pos) = Ty.INT)
                                            then true
                                            else false
        | (Ty.ARRAY (ty,_), Ty.STRING)  => if ((actualTy ty pos) = Ty.STRING)
                                            then true
                                            else false
        | _                              => ((errorAssignable(pos,msg)); false)
end

fun findID fields id pos =
    case fields of  
        []                   => ((errorNameNotDeclared(pos,id)); Ty.ERROR)
      | (symbol, type')::rest => if (S.name id = S.name symbol)      
                                then type'
                                else findID rest id pos

fun transTy (tenv, t) = 
    let
        fun transFieldData tenv fielddata =
            case fielddata of
                [] => []
              | {name, escape, typ = (symbol, tpos), pos}::rest => (name, actualTy (lookupTy tenv symbol pos) pos)::transFieldData tenv rest
    in
        case t of 
            A.NameTy(symbol, pos) => lookupTy tenv symbol pos 
          | A.ArrayTy(symbol, pos) => Ty.ARRAY (lookupTy tenv symbol pos, ref())
          | A.RecordTy(fielddata) => Ty.RECORD (transFieldData tenv fielddata, ref()) 
        
    end

fun transExp (venv, tenv, extra : extra) =
    let
        (* this is a placeholder value to get started *)
        val TODO = {exp = TAbs.ErrorExp, ty = Ty.ERROR}

        fun   trexp (A.NilExp) = { exp = TAbs.NilExp, ty = Ty.NIL }
            | trexp (A.VarExp var) =  trvar var
	        | trexp (A.IntExp n) = { exp = TAbs.IntExp n, ty = Ty.INT }
	        | trexp (A.StringExp (s,pos)) = { exp = TAbs.StringExp s, ty = Ty.STRING }
            | trexp (A.LetExp {decls, body, pos} ) =  
                let val  {decls = result, venv = venv', tenv = tenv'} = 
                            transDecs(venv, tenv, decls, extra)
                    val transexp =  transExp(venv', tenv', extra) body
                in 
                    {exp = TAbs.LetExp {decls = result, body = transexp}, ty = #ty transexp}
                end 

            | trexp ( A.OpExp {left, oper, right, pos} ) =
            let val left_ta  = trexp left
                val right_ta = trexp right
                val left_ty = (#ty left_ta)
                val right_ty = (#ty right_ta)
                val oper' = (case oper of
                        A.EqOp =>  TAbs.EqOp
                      | A.PlusOp => TAbs.PlusOp
                      | A.MinusOp =>TAbs.MinusOp
                      | A.DivideOp => TAbs.DivideOp
                      | A.TimesOp => TAbs.TimesOp
                      | A.NeqOp => TAbs.NeqOp

                      | A.LtOp => TAbs.LtOp
                      | A.GtOp => TAbs.GtOp
                      | A.LeOp => TAbs.LeOp
                      | A.GeOp => TAbs.GeOp
                      | A.ExponentOp => TAbs.ExponentOp)
            in
                if (oper'=TAbs.LtOp orelse 
                    oper'=TAbs.GtOp orelse 
                    oper'=TAbs.LeOp orelse 
                    oper'=TAbs.GeOp)
                then

                    if (left_ty = right_ty) andalso (left_ty = Ty.INT orelse left_ty = Ty.STRING)
                    then  
                        {exp = TAbs.OpExp{ left = left_ta
                                         , oper = oper'
                                         , right = right_ta}
                        ,ty = Ty.INT}
                    else if left_ty <> right_ty 
                    then 
                        (errorOpSides (pos, left_ty, right_ty); errorExp)
                    else 
                        (errorIntorString (pos, left_ty); {exp = (TAbs.NilExp), ty = Ty.ERROR})
               
                else if (oper'=TAbs.EqOp orelse 
                    oper'=TAbs.NeqOp)

                then
                    if (left_ty = right_ty)
                    then  {exp=TAbs.OpExp{left = left_ta, oper = oper', right = right_ta},ty=Ty.INT}
                    else (errorOpSides (pos, left_ty, right_ty); errorExp)

                else
                    if (left_ty = right_ty) andalso (left_ty = Ty.INT)
                    then {exp=TAbs.OpExp{left = left_ta, oper = oper', right = right_ta},ty=Ty.INT}
                    else if left_ty <> right_ty then (errorOpSides (pos, left_ty, right_ty); errorExp)
                    else (errorInt (pos, left_ty); {exp = (TAbs.NilExp), ty = Ty.ERROR})
            end
            | trexp (A.RecordExp {fields, typ, pos}) = 
                let 
                    fun trfields fields =
                        case fields of 
                            [] => []
                          | (symbol, exp, pos)::rest => 
                            let
                                val exp' = trexp exp
                            in
                                (symbol, exp')::trfields rest 
                            end
                    val transfields = trfields fields
                in
                    {exp = TAbs.RecordExp {fields = transfields}, ty = actualTy (lookupTy tenv typ pos) pos}
                end
            | trexp ( A.SeqExp seqExp ) = 
                let 
                    fun findTy seqexp =
                        case seqexp of
                            []             => Ty.UNIT
                          | (exp, pos)::[] => 
                                let
                                    val exp' = trexp exp
                                in
                                    (#ty exp')
                                end
                          | (exp, pos)::rest => findTy rest

                    fun transSeqExp seqexp =
                        case seqexp of
                            [] => []
                          | (exp,pos)::rest => trexp exp::transSeqExp rest

                    val tSeqExp = transSeqExp seqExp
                    val seqExpTy = findTy seqExp
                in
                    {exp = TAbs.SeqExp (tSeqExp), ty = seqExpTy}    
                end
              (* Add errors. Der må ikke være et nil i midten af sekvensen.*)
            | trexp (A.AssignExp {var, exp, pos}) =  
                let 
                    val {exp = TAbs.VarExp var', ty = ty'} = trvar var    
                    val exp' = trexp exp
                    val varty = actualTy ty' pos
                    val expty = actualTy (#ty exp') pos
                    val loopvar = (case #loopvar extra of
                        SOME (loopvar) => loopvar
                      | NONE =>  (Symbol.symbol "visatserbarepådennevarikkefindesiprogrammetnogensindeihelelivet133742069"))
                    fun assignvar var' = case var' of 
                        {var = TAbs.SimpleVar id, ty = _} => id
                      | {var = TAbs.FieldVar (fvar, id), ty = _} => assignvar fvar
                      | {var = TAbs.SubscriptVar (svar, texp), ty = _} => assignvar svar
                    
                    (*val {exp = TAbs.VarExp var', ty = ty'} = trvar var*)
                in 
                    
                    (if (checkAssignable (varty, expty, pos, "Assignment and declerationtype does not match")) 
                    then 
                        (if (assignvar var') = (loopvar)
                        then 
                            (errorMayNotAssignToLoopVar(pos); errorExp)
                        else
                            {exp = TAbs.AssignExp{var = var', exp = exp'}, ty = Ty.UNIT})
                    else 
                        (errorAssignTypeMismatch(pos,varty,expty);errorExp))
                    
                        
                end
                
            | trexp (A.IfExp {test = test, thn, els = els', pos} )
            
            (* action list:                                            *)
            (* 1. type-check test expression and each of the branches *)
            (* 2. check that test is integer                           *)
            (* 3. check that branches agree on their types             *)
            (* 4. construct a value of TAbs type and return it         *)

                = let val test' as {exp = _, ty = test_ty  } = trexp test
                      val thn'  as {exp = _, ty = ty_thn'  } = trexp thn
                      val els'' = (case els' of NONE => {exp=TAbs.NilExp, ty = Ty.UNIT}
                                                |SOME el => trexp el)
                      val ty_els'' = #ty els''
                      val actest = actualTy test_ty pos
                      val acthen = actualTy ty_thn' pos
                      val acelse = actualTy ty_els'' pos
                in
                      if acthen = acelse andalso actest = Ty.INT then
                        (* all good; return *)
                      
                        { exp = TAbs.IfExp{test = test', thn = thn', els = SOME els'' }
                            , ty  = ty_thn'  }
                      else
                          if ty_thn' <> ty_els'' then (errorThenElse (pos, ty_thn', ty_els''); 
                          { exp = TAbs.NilExp, ty  = Ty.ERROR  })
                      else (errorInt (pos, test_ty); errorExp)
                end
            | trexp (A.WhileExp {test, body, pos}) = 
	    (* Check that the type of exp is int *)
	    (* Check that the type of body is unit *)
	            let
		            val {exp = testexp, ty = whiletype} = trexp test
		            val {exp = bodyexp, ty = bodytype} = transExp(venv, tenv, {inloop = ref true, loopvar = NONE}) body
	            in
		            if (whiletype = Ty.INT)
		            then
		                (if (bodytype = Ty.UNIT)
		                then
			                {exp = TAbs.WhileExp {test = {exp = testexp, ty = whiletype}, body = {exp = bodyexp, ty = bodytype}}, ty = Ty.UNIT}
		                else
			                (errorWhileBody (pos, bodytype); errorExp))
		            else
		                (errorWhileCheck (pos, whiletype); errorExp)
	            end 
            | trexp (A.ForExp {var,escape,lo,hi,body,pos}) =
	            let
		            val loexp' as {exp = loexp, ty = lotype } = trexp lo
		            val hiexp' as {exp = hiexp, ty = hitype } = trexp hi
		            val newEnv = S.enter(venv, var, Env.VarEntry {ty = Ty.INT})
		            val bodyexp' as {exp = bodyexp, ty = bodytype} = transExp(newEnv, tenv, {inloop = ref true, loopvar = SOME var }) body
	            in
		            if (lotype <> Ty.INT) then
		                (errorForLo(pos, lotype); {exp=TAbs.NilExp, ty = Ty.ERROR})
		            else if (hitype <> Ty.INT) then
		                (errorForHi(pos, hitype); {exp=TAbs.NilExp, ty = Ty.ERROR})
		            else if (bodytype <> Ty.UNIT) then
		                (errorForBody(pos, bodytype); {exp=TAbs.NilExp, ty = Ty.ERROR})
		            else
		                {exp = TAbs.ForExp {var = var, escape = escape, lo = loexp', hi = hiexp', body = bodyexp'}, ty = Ty.UNIT}
	            end
		  
            | trexp (A.BreakExp pos) =
	            let
		            val inloop = (#inloop extra)
	            in
		            if (!inloop <> true) 
                    then
		                (errorLoopBreak(pos); errorExp)
		            else
		                {exp = TAbs.BreakExp, ty = Ty.UNIT}
	            end
            | trexp (A.ArrayExp {typ, size, init, pos}) = 
                (case S.look(tenv,typ) of
                            NONE => (errorTypeNotInEnv(pos, typ); errorExp)
                            | SOME (t) => (case (actualTy t pos) of
                                Ty.ARRAY(arrtype,_) =>
                                    let
                                    val size' = trexp size
                                    val sizetype = actualTy (#ty size') pos
                                    val init' = trexp init
                                    val initType = actualTy (#ty init') pos
                                    in
                                        (compareTy(arrtype,initType,pos,tenv);
                                        checkInt(sizetype,pos);
                                        {exp=TAbs.ArrayExp{size = size', init = init'},ty=t})
                                    end
                                | _ => (errorNotAnArray(pos, t);errorExp)))

            | trexp (A.CallExp ({func, args, pos})) = 
                (case S.look (venv, func) of 
                    SOME (E.VarEntry{ty}) => (errorVarNotFun(pos,ty); {exp=TAbs.NilExp, ty = Ty.ERROR})
                    | NONE => (errorFunNotFound(pos, S.name(func)); {exp=TAbs.NilExp, ty = Ty.ERROR})
                    | SOME (E.FunEntry{formals,result}) => 
                    if length(args) <> length(formals) then (errorFunArgs(pos,length(args),length(formals));{exp=TAbs.NilExp, ty = Ty.ERROR}) else if
                    compareTys(formals, args, pos, tenv) then
                    {exp = TAbs.CallExp{func=func , args= trArgs args}, ty = result} else
                    (errorFunTypeMismatch(pos); errorExp) ) 

        and compareTys (tyList, arglist, pos, tenv) = 
            case tyList of
            [] => true
            |(ty1::tyrest) => if (compareTy (ty1, #ty (trexp (#1 (hd arglist))), pos, tenv)) then compareTys (tyrest, tl arglist, pos, tenv) else false

        and trArgs (args) = case args of
                    [] => []
                    | (a::rest) => trexp (#1 a)::trArgs(rest)

        and trvar (A.SimpleVar (id, pos)) = 
            let 
                val ty = (case S.look (venv,id) of 
                          SOME (E.VarEntry {ty}) => ty 
                        |  _ => (errorFunctionNotVar(pos,id); Ty.ERROR))
			in
                {exp = TAbs.VarExp {var = TAbs.SimpleVar id, ty = ty}, ty = (actualTy ty pos)}
		    end
          | trvar (A.FieldVar (var, id, pos)) = 
                let 
                    val {exp = TAbs.VarExp var', ty = ty'} = trvar var
                    val varTy = (case ty' of
                                    Ty.RECORD (fields, recTy) => findID fields id pos
                                  | _ => (errorFunNotRecord(pos); Ty.ERROR))
                in
                
                    (case varTy of
                        Ty.ERROR => errorExp
                      | _ => {exp = TAbs.VarExp {var = TAbs.FieldVar (var', id)
                                                , ty = varTy}
                             , ty = actualTy varTy pos})
                end
          | trvar (A.SubscriptVar (var, exp, pos)) = 
                let   
                    val tvar as {exp = TAbs.VarExp var', ty = ty'} = trvar var
                    val (texp as {exp = exp', ty = varty}, ty) = 
                        (case ty' of
                            Ty.ARRAY (ty, rf) => (trexp exp, ty)
                          | _                 => (errorVarNotArray(pos); ({exp = TAbs.ErrorExp, ty = Ty.ERROR}, ty')))
                    
                in
                    case varty of   
                        Ty.INT => {exp = TAbs.VarExp {var = TAbs.SubscriptVar (var', texp)
                                                     , ty = actualTy ty pos}
                                  , ty = actualTy ty pos}
                      | _      => (errorExpNotInt(pos); {exp = TAbs.ErrorExp, ty = Ty.ERROR})    
                end
    in
        trexp
    end

and transDec ( venv, tenv
             , A.VarDec {name, escape, typ = NONE, init, pos}, extra : extra) =
             let 
                val {exp,ty} = transExp(venv, tenv, extra) init
                val venv' = S.enter(venv, name, E.VarEntry{ty=ty})
             in 
                if (ty = Ty.NIL)
                then (errorInitNil(pos); {decl = TAbs.VarDec {name = name
                                    , escape = escape
                                    , ty = ty
                                    , init = errorExp}
                        , tenv = tenv
                        , venv = venv'}) 
                else
                {decl = TAbs.VarDec {name = name
                                    , escape = escape
                                    , ty = ty
                                    , init = {exp = exp ,ty = ty}}
                        , tenv = tenv
                        , venv = venv'}
            end 

  | transDec ( venv, tenv
             , A.VarDec {name, escape, typ = SOME (symbol, pos), init, pos=pos1}, extra) =
             let 
                val {exp,ty} = transExp(venv,tenv,extra) init
                val constraintTy = S.look(tenv, symbol)
            in
                (case constraintTy of
                    NONE => (errorConstraintTypeNotFound(pos, symbol); {decl = TAbs.VarDec {name = name
                                    , escape = escape
                                    , ty = Ty.ERROR
                                    , init = errorExp}, tenv = tenv, venv = venv}) 
                  | SOME t => (checkAssignable(t, ty, pos1, ("Typeannotation and bodytype do not match")); 
                                {decl = TAbs.VarDec {name = name
                                                    , escape = escape
                                                    , ty = ty
                                                    , init = {exp = exp, ty = ty}}
                                , tenv = tenv
                                , venv = S.enter(venv, name, E.VarEntry{ty = actualTy ty pos})}))
            end

  | transDec (venv, tenv, A.TypeDec tydecls, extra) = 
    let
        (* Make lists og names and types *)
        val nameList = map #name tydecls
        val typeList = map #ty tydecls
        val pos = #pos (hd tydecls)
        (* First iteration where we insert the names*)
        fun insertNames (name, env) =  
            (case S.look(env, name) of  
                NONE => S.enter(env, name, Ty.NAME(name, ref(NONE)))
              | SOME _ => (errorTypeExistsAlready(pos,name); env))
        val tenv' = foldr insertNames tenv nameList

        (* Update the refs*)
        fun transTydecl tydecls tenv =
            case tydecls of 
                [] => []
              | {name = name', ty = ty', pos = pos'}::rest =>
                    let
                        val trTy = transTy (tenv, ty')
                        val tyOpt = lookupTy tenv name' pos'
                    in
                        (case tyOpt of
                            Ty.NAME (_,tyRef) => tyRef := SOME (actualTy trTy pos')
                          | _ => (errorRedundant(pos')));
                          
                        {name = name', ty = trTy}::transTydecl rest tenv
                    end
        val transdecls = transTydecl tydecls tenv'
        fun isNotCyclic (tenv, symbolNames, resNames) =
            (case symbolNames of
            [] => true
          | _  => 
                let 
                    val name = hd symbolNames
                    val res' = tl symbolNames
                    val ty   = S.look (tenv, name)
                in
                    (case ty of
                        SOME(Ty.NAME(sym1, type1)) => 
                            (case !type1 of  
                                SOME(Ty.NAME(sym2, type2)) => 
                                    (if (List.exists (fn element => element = name) resNames)
                                    then 
                                        false
                                    else 
                                        (isNotCyclic(tenv, [sym2], resNames@[name])))
                              | _ => isNotCyclic (tenv, res', []))
                            
                      | _ => isNotCyclic (tenv, res', []))
                end)
        
    in
        (if (isNotCyclic (tenv', nameList, []))
        then 
            ((* Everything is fine *))
        else 
            (errorCycleInDecs (pos));
        {decl = TAbs.TypeDec transdecls, tenv = tenv', venv = venv}) 
        
    end
    

  | transDec (venv, tenv, A.FunctionDec fundecls, extra) =
    let
        fun updateEnv env fundecls = 
            (case fundecls of 
                [] => env
              | { name = name'
                , params = fielddata
                , result = result' 
                , body = bodyExp
                , pos = pos'}::rest => 
                    let
                        val resultType =
                            (case result' of
                                NONE => Ty.UNIT
                              | SOME (symbol, pos) => (actualTy (lookupTy tenv symbol pos) pos))
                        val (venv', _) = transfielddata env fielddata
                        val fielddatatys = getTys fielddata
                        val funentry = E.FunEntry{formals = fielddatatys, result = resultType}
                        val venv'' = S.enter(venv', name', funentry)
                    in
                        updateEnv venv'' rest 
                    end)
        and transfielddata env fieldata = 
            case fieldata of    
                [] => (env, [])
              | { name = name'
                , escape = escape'
                , typ = (symbol, pos1)
                , pos = pos2 }::rest =>
                    let     
                        val ty' = (actualTy (lookupTy tenv symbol pos1) pos2)
                        val venv' = S.enter(env, name', E.VarEntry{ty = ty'})
                        val (venv'', res') = transfielddata venv' rest
                    in
                        (venv'',
                        { name = name'
                        , escape = escape'
                        , ty = ty'}::res')
                    end
        and getTys fielddata =
            case fielddata of 
                [] => []
              | { name = name'
                , escape = _
                , typ = (symbol, pos1)
                , pos = pos2}::rest =>
                    let
                        val ty = (actualTy (lookupTy tenv symbol pos1) pos2)
                    in
                        ty::getTys rest
                    end
        and transFunDecls env fundecls = 
            case fundecls of 
                [] => []
              | { name = name'
                , params = fielddata
                , result = result'
                , body = bodyexp
                , pos = pos'}::rest => 
                    let 
                        val resultTy = 
                            (case result' of
                                NONE => Ty.UNIT
                              | SOME (symbol,pos) => (actualTy (lookupTy tenv symbol pos') pos'))
                        val (venv', tfielddata) = transfielddata env fielddata
                        val texp as {exp = _, ty = bodyExpTy} = (transExp (venv', tenv, extra)) bodyexp
                    in
                        if
                            isAlreadyDeclared (name', rest)
                        then 
                            errorNameAlreadyDeclared(pos', name')
                        else ();
			checkForParamNameDuplicates(fielddata);
                        if ((resultTy = Ty.UNIT andalso bodyExpTy <> Ty.UNIT) orelse (resultTy <> Ty.UNIT andalso bodyExpTy = Ty.UNIT))
                        then errorReturnTypeMismatch(name', bodyExpTy, resultTy, pos')
                        else if 
                            compareTy (bodyExpTy, resultTy, pos' , tenv)
                        then
                            ()
                        else (errorReturnTypeMismatch(name', bodyExpTy,resultTy, pos'));
                        (* Check for result types*)
                        { name = name'
                        , params = tfielddata
                        , resultTy = resultTy
                        , body = texp}::transFunDecls env rest
                    end
            and isAlreadyDeclared (name, list) =
                case list of 
                    [] => false
                  | { name = name'
                    , params
                    , result
                    , body
                    , pos}::rest =>
                        if (S.name name = S.name name')
                        then true
                        else isAlreadyDeclared (name, rest)
	    and checkForParamNameDuplicates functionParams =
		case functionParams of
		    [] => ()
		  | { name = name', escape = escape', typ = typ', pos = pos'}::rest => if (nameInList (S.name name') rest) then duplicateNameInFunctionCall(pos', name') else ()
	    and nameInList name params =
		case params of
		    [] => false
		  |  { name = name', escape = escape', typ = typ', pos = pos'}::rest => if (name = S.name name') then true else nameInList (S.name name') rest
		    
        val venv' = updateEnv venv fundecls
        val transfundecls = transFunDecls venv' fundecls
            
        (* val transdecls = transFunDec venv' fundecls *)
    in
        {decl = TAbs.FunctionDec transfundecls, tenv = tenv, venv = venv'} 
    end

and transDecs (venv, tenv, decls, extra : extra) =
    let fun visit venv tenv decls result =
            case decls
             of [] => {decls = result, venv = venv, tenv = tenv}
              | (d::ds) =>
                let
                    val { decl = decl
                        , venv = venv'
                        , tenv = tenv'} = transDec (venv, tenv, d, extra)
                in
                    visit venv' tenv' ds (result @ (decl :: []))
                end
    in
        visit venv tenv decls []
    end

fun transProg absyn = 
    let
    in
        transExp (Env.baseVenv, Env.baseTenv, {inloop = ref false, loopvar = NONE }) absyn
    end
    

end (* Semant *)

(*Spørgsmål til Ben:
ActualTy? Hvordan havde i tænkt den skulle implementeres? OGså de andre hjælpe metoder
Hvad gør vi med returtype når vi finder en fejl? Analysen skal vel fortsætte? Eller skal den?
Constrainttypen; Ligger den i venv eller tenv?
Når ting skal tilføjes til environments er så rigtigt som vi har gjort? ØØØ venv = S.enter(venv, name, E.VarEntry{ty=ty} ØØØ
Tager han imod mails i ferien?? :))))
Der må ikke være et nil i midten af sekvensen. (?? hvordan)

*)
    
