(* Aarhus University, Compilation 2016  *)
(* DO NOT DISTRIBUTE                    *)
(* DO NOT CHANGE THIS FILE              *)


(* LocalOS - a helper file to extract OS information *)

structure LocalOS = struct

(* Darwin/OSX support is a bonus; The official course VM is Linux *)
datatype OS = Linux | Darwin
exception OSNotSupported

val os =
    let fun checkOS (s) =
          OS.Process.system ("uname | grep -q " ^ s)
          = OS.Process.success
    in if checkOS "Linux" then Linux
       else if checkOS "Darwin" then Darwin
       else raise OSNotSupported
    end
end (* Local OS *)
