(* Aarhus University, Compilation 2018  *)
(* DO NOT DISTRIBUTE                    *)
(* SKELETON FILE –                      *)


(* LLCodegen -- Tiger to LLVM-- code generator *)


signature LLCODEGEN =
sig
  val codegen_prog : { exp : OAbsyn.exp , locals: (int * Types.ty) list } -> ll.prog
  val codegen_ll  :  { exp : OAbsyn.exp , locals: (int * Types.ty) list } -> string
end


structure LLCodegen :> LLCODEGEN =
struct

structure A'' = OAbsyn
structure Ty = Types
structure S = Symbol
structure B = CfgBuilder
structure E = CgEnv


exception CodegenFatal (* should not happen *)

exception NotImplemented
fun TODO() = (print "Hit a TODO\n"; raise NotImplemented)


(* variable environment is a mapping of symbols to variable entries in the
coge generation environment  *)

type venv = E.enventry S.table
type tenv = (Ty.ty * ll.ty) S.table


(* representation of the locals context

Given a Tiger code with nested declarations

let function fun1 (...) =
    let function fun2 (...) =
    in ...
    end
in
   ...
end

we represent it at runtime with the locals list of type (ll.tid * ll.ty) list
that might looks like this:

[ (%typeLocalsMain, { LL struct for Main's locals } )
, (%typeLocalsFun1, { LL struct for Fun1's locals } )
, (%typeLocalsFun2, { LL struct for Fun2's locals } ]
]

Here %typeLocalMain, %typeLocalsFun1, etc are LL types that need to be emitted;
the LL structs contain the information about the LL records about the local
variables in each function. The length of the list corresponds to the current
of nested declarations.*)

type cg_ctxt = { venv : venv
               , tenv : tenv
               , locals  : (ll.tid * ll.ty) list  (* the locals *)
               , lid : ll.uid (* the llvm structure keeping the
                                           locals of the current function *)
                                     (* ... there is probably more *)
                (* For detecting merg_lbl when making breakExps*)
               , merg_lbl : ll.lbl option
               }


(* tranlate Tiger type to LL type *)
fun ty_to_llty ( ty: Ty.ty) : ll.ty =
  case ty
   of Ty.NIL                    => ll.Ptr ll.I8
    | Ty.INT                    => ll.I64
    | Ty.STRING                 => ll.Ptr (ll.I8)
    | Ty.ARRAY _                => ll.Ptr (ll.I8)
    | Ty.RECORD _               => ll.Ptr (ll.I8)
    | Ty.NAME (_, ref (SOME t)) => ty_to_llty t
    | Ty.UNIT                   => ll.Void
    | _                         => raise CodegenFatal

(* -------------------------------------------------- *)
(* The result of code generation for expressions is an LL operand
   paired with a function from CFG builders to CFG builders *)


(* -- A brief introduction to Buildlets ------------- 


One of the challenges of generating LLVM code from a tree-like
representation such as Tiger ASTs is that while AST expressions are
tree-like, the LL code is linear. That means that the order in which
we translate different Tiger subexpressions starts to matter.

The "buildlet" approach separates the challenge of code generation for
a given a Tiger expression into two mostly orthogonal concerns:

Concern 1: What is the code for each of the subexpressions?

Concern 2: What is the order in which the code for the subexpressions
must be put together in order to produce the correct code for the
parent expression?

To implement this separation of concerns, we design the translation
function to be a function from a Tiger AST to *a CFG transformer*. CFG
transformers themselves are functions that take a CFG builder
(representing the state of the CFG at the time of putting the LL code
together) as an argument and return a CFG builder (the modified CFG).
The idea is that each subexpression transformer "knows" how to
generate the code for that subexpression, and can be invoked in a
black-box manner as long as we give it the state of the current
translation as an argument.

This approach allows us to translate the subexpressions (concern 1) in
a way that allows us to delay the decision of how to put them together
(concern 2). The benefit is that we can be compositional when working
with the transformers, as the code tends to be a lot less fragile.

----------------------------------------------------- *)


(* we call our CFG transformers buildlets *)
                          
type buildlet = B.cfg_builder -> B.cfg_builder

(* the operand expresses where to find the result assuming that
   the buildlet is invoked *)
type cg_res = (ll.operand * buildlet)


(* some auxiliary functions *)

fun emit_insn insn =
  fn b => B.add_insn b insn

fun emit_term tr =
  fn b => B.term_block b tr

fun emit_lbl lbl =
  fn b => B.set_label b lbl

fun emit_alloca t =
  fn b => B.add_alloca b t

val id_buildlet = fn b => b

fun seq_buildlets (bls: buildlet list) :buildlet =
  fn cfgb_init =>
     List.foldl
         (fn (b, cfgb') => b cfgb') (* apply the next buildlet to the threading CFGBuilder *)
         cfgb_init
         bls


fun build_cfg buildlet = B.get_cfg (buildlet (B.new_env()))

val build_cfg_from_buildlets = (build_cfg o seq_buildlets)

fun mapseq f ls = seq_buildlets (map f ls)


fun emit_uid' (a, b) = emit_insn (SOME a, b)
fun build_store args = emit_insn (NONE, ll.Store args)

(* -------------------------------------------------- *)


val mk_uid = LLEnv.mk_uid
val add_named_type = LLEnv.add_named_type
val mk_label_name  = LLEnv.mk_label_name
val mk_type_name  = LLEnv.mk_type_name
val add_gid_decl = LLEnv.add_gid_decl
val mk_gid = LLEnv.mk_gid

fun emit_mk' ll_env (s, i) =
  let val uid = mk_uid ll_env s
  in (uid, emit_uid' (uid,i))
  end

fun emit_mk'' ll_env (s, i) =
  let val uid = mk_uid ll_env s
  in (ll.Id uid, emit_uid' (uid,i))
  end

fun build_load_op ll_env s args =
  let val uid = mk_uid ll_env s
  in (ll.Id uid, emit_uid' (uid, ll.Load args))
  end

(* -------------------------------------------------- *)

(* our main workhorse function for code generation *)
fun cgExp ll_env (cg_ctxt:cg_ctxt) (aug_exp:A''.exp) : cg_res =
  let val exp = #exp aug_exp
      val ty = (#ty o #aug) aug_exp
      val emit_mk'  = emit_mk' ll_env
      val emit_mk'' = emit_mk'' ll_env
      val build_load_op = build_load_op ll_env


      (* convenient function for recursive calls when the
         environment and the context do not change *)
      val (cgE : (A''.exp -> cg_res)) = cgExp ll_env cg_ctxt

  in case exp
      of A''.NilExp   => ( ll.Null, id_buildlet) (* returned operand should be ll.Null      *)

       | A''.IntExp i => ( ll.Const i, id_buildlet)


       | A''.OpExp {left, oper, right} => 
                                            let 
                                                val _ = print("Vi kom ind \n")
                                                val (leftOperand, build_left) = cgE left
                                                val (rightOperand, build_right) = cgE right
                                                val expType = (#ty (#aug left))
                                                val isStringType = if (expType = Types.STRING) then true else false
                                                val dummy_static_link = mk_gid ll_env "dummy"
                                                val dummyVal = "hej"
                                                      
                                                val _ = print("Vi kom igennem cg_exp \n")

                                                fun intBinop oper' = 
                                                    let                                    
                                                        val (insn, instype) = (case oper' of
                                                                A''.PlusOp => (ll.Binop(ll.Add, ll.I64, leftOperand, rightOperand),"binop")
                                                                | A''.MinusOp =>(ll.Binop(ll.Sub, ll.I64, leftOperand, rightOperand),"binop")
                                                                | A''.TimesOp => (ll.Binop(ll.Mul, ll.I64, leftOperand, rightOperand),"binop")
                                                                | A''.DivideOp => (ll.Binop(ll.SDiv, ll.I64, leftOperand, rightOperand),"binop")
                                                                | A''.EqOp => (ll.Icmp(ll.Eq, ll.I1, leftOperand, rightOperand),"icmp")
                                                                | A''.NeqOp => (ll.Icmp(ll.Ne, ll.I1, leftOperand, rightOperand),"icmp")
                                                                | A''.LtOp => (ll.Icmp(ll.Slt, ll.I1, leftOperand, rightOperand),"icmp")
                                                                | A''.LeOp => (ll.Icmp(ll.Sle, ll.I1, leftOperand, rightOperand),"icmp")
                                                                | A''.GtOp => (ll.Icmp(ll.Sgt, ll.I1, leftOperand, rightOperand),"icmp")
                                                                | A''.GeOp => (ll.Icmp(ll.Sge, ll.I1, leftOperand, rightOperand),"icmp")
                                                                | _ => (ll.Binop(ll.Add, ll.I64, leftOperand, rightOperand),"binop")) (*volapyk, slet når alle options er exhausted*)
                                                        val (uid, b) = (emit_mk'  ("binop", insn))
                                                        val (tytrans_oper,tytrans_build) = emit_mk'' ("tytrans"
			                                                                                                , ll.Zext ( ll.I1
				                                                                                                          , ll.Id uid
				                                                                                                          , ll.I64)
				                                                                                            )
                                                        val (buildletlist, address) = (case instype of 
                                                                "icmp" => ([build_left, build_right, b, tytrans_build], tytrans_oper)
                                                              | _      => ([build_left, build_right, b], ll.Id uid))
                                                    in 
                                                        (buildletlist, address)
                                                    end
                                                
                                                fun stringBinop oper' =
                                                    let
                                                        val gid = mk_gid ll_env ("stringEqual")
                                                        val insn = (case oper' of    
                                                            A''.EqOp => (ll.Call (ll.I64, ll.Gid (S.symbol "stringEqual") ,[(ll.Ptr ll.I8, leftOperand),(ll.Ptr ll.I8, rightOperand)]))
                                                         |  A''.NeqOp => (ll.Call (ll.I64, ll.Gid (S.symbol "stringNotEq") ,[(ll.Ptr ll.I8, leftOperand),(ll.Ptr ll.I8, rightOperand)]))
                                                         |  A''.LtOp => (ll.Call (ll.I64, ll.Gid (S.symbol "stringLess") ,[(ll.Ptr ll.I8, leftOperand),(ll.Ptr ll.I8, rightOperand)]))
                                                         |  A''.LeOp => (ll.Call (ll.I64, ll.Gid (S.symbol "stringLessEq") ,[(ll.Ptr ll.I8, leftOperand),(ll.Ptr ll.I8, rightOperand)]))
                                                         |  A''.GtOp => (ll.Call (ll.I64, ll.Gid (S.symbol "stringGreater") ,[(ll.Ptr ll.I8, leftOperand),(ll.Ptr ll.I8, rightOperand)]))
                                                         |  A''.GeOp => (ll.Call (ll.I64, ll.Gid (S.symbol "stringGreaterEq") ,[(ll.Ptr ll.I8, leftOperand),(ll.Ptr ll.I8, rightOperand)]))
                                                         (* Concat virker ikke da jeg ikke ved hvad staticlink skal være *)
                                                         (*|  A''.PlusOp => (ll.Call (ll.I64, ll.Gid (S.symbol "concat") ,[(ll.Ptr ll.I8, ll.Gid dummy_static_link),(ll.Ptr ll.I8, leftOperand),(ll.Ptr ll.I8, rightOperand)]))*)
                                                         | _ => (print("StringOp failed"); raise CodegenFatal)
                                                            )

                                                        val (uid, build) = (emit_mk' ("stringEqOp", insn))
                                                        val buildletlist =  [
                                                                        build_left
                                                                      , build_right
                                                                      , build
                                                        ]
                                                    in
                                                        (buildletlist, ll.Id uid)
                                                    end

                                                val (buildletlist, address) = if isStringType then stringBinop oper else intBinop oper
                                                val b_ = seq_buildlets buildletlist
                                            in 
                                                (address, b_)
                                            end
       | A''.SeqExp [e]                => cgE e
       | A''.SeqExp exps               => let
                                            val (operands, buildlets) = ListPair.unzip (map cgE exps )
                                          in
                                            (List.last operands, seq_buildlets buildlets)
                                          end


       | A''.LetExp {decls, body}      => (* needs to call cgDecl: den returnerer en opdateret cg_ctxt og instrukser du skal bruge. 
                                                    Den skal du bruge til at beregne bodyen på. *)
                                          let
                                            val (ny_ctxt, buildlet_decl) = cgDecls ll_env cg_ctxt decls 
                                            val (oper_body, buildlet_body) = cgExp ll_env ny_ctxt body 
                                            val b_ = seq_buildlets [buildlet_decl, buildlet_body]
                                          in
                                            (oper_body, b_)
                                          end 
       | A''.VarExp var                => cgVar ll_env cg_ctxt var
                                            (* delegate to a dedicated function *)
       | A''.AssignExp {var, exp}      => let 
                                            val (exp_oper, exp_build) = cgE exp
                                            val (varop,vartype,varbuildlet) = cgVarLoc ll_env cg_ctxt var
                                            val b = (build_store (vartype,exp_oper,varop))
                                            val b_ = seq_buildlets [exp_build, varbuildlet,b]
                                          in
                                            (ll.Null,b_)
                                          end

       | A''.IfExp {test, thn, els as (SOME els') }  =>
         let 
             val then_lbl = mk_label_name ll_env "then"
             val else_lbl = mk_label_name ll_env "else"
             val merg_lbl = mk_label_name ll_env "merge"
             
             (* Check if the return type of thn els is UNIT *)
             val isEmptyThenElse = if (#ty (#aug thn)) = Types.UNIT then (print("Return is empty \n"); true) else false
             val (test_oprnd, test_b) = cgE test
             val _ = print("After test oper \n")
             
             val (then_oprnd, then_b) = if (not isEmptyThenElse) then cgE thn else (ll.Null, id_buildlet)
             val _ = print("After then oper \n")
             val (else_oprnd, else_b) = if (not isEmptyThenElse) then cgE els' else (ll.Null, id_buildlet) 
             val _ = print("After else oper \n")


             (* branch on test_oprnd, and depending on the branch,
                jump to either then_lbl or else_lbl *)

             val (test_op', build_icmp) =
		      emit_mk'  ("test"
			   , ll.Icmp ( ll.Ne
				     , ll.I64
				     , test_oprnd
				     , ll.Const 0))

             val build_cbr =
                 emit_term (ll.Cbr ( ll.Id test_op'
				   , then_lbl
				   , else_lbl ))

             val uid = mk_uid ll_env "ifthenelse_result"
             val llty = ty_to_llty ty
             val b_res_alloca = if (not isEmptyThenElse) then emit_alloca (uid, llty) else (id_buildlet)
             fun b_store_r r = if (not isEmptyThenElse) then build_store (llty, r , ll.Id uid) else (id_buildlet)

	     	 val (res_val, b_load) =
    		 build_load_op "res" (llty, ll.Id uid)
       		 val b_load = if (not isEmptyThenElse) then b_load else (id_buildlet)

	     	 val b_br_merge = emit_term (ll.Br merg_lbl)

             val b_ = seq_buildlets [
                     b_res_alloca

                   , test_b
                   , build_icmp
                   , build_cbr

                   , emit_lbl then_lbl
                   , then_b
                   , b_store_r then_oprnd
                   , b_br_merge

                   , emit_lbl else_lbl
                   , else_b
                   , b_store_r else_oprnd
                   , b_br_merge

                   , emit_lbl merg_lbl
                   , b_load ]
         in
             (res_val, b_) (* *)
         end

       | A''.CallExp {func, args}                    =>  
                let
                    val venv = #venv cg_ctxt
                    val f = (case S.look(venv,func) of SOME(E.FunEntry e) => e
                                                                   |_ => (print("function does not exist in venv"); raise CodegenFatal))
                    val (ops,builds) = ListPair.unzip (map cgE args)
                    val retty = #result f

                    fun findtypes (e:A''.exp) : ll.ty = ty_to_llty ((#ty o #aug) e)
                    val argtypes = map findtypes args

                    fun pairopty argop argty : (ll.ty * ll.operand) = (argty, argop)
                    fun makearglist oplist arglist : (ll.ty * ll.operand) list = let 
                                                                                    val oper = List.hd oplist
                                                                                    val typ = List.hd arglist
                                                                                    val rest = case List.tl oplist of [] => []
                                                                                                                      |_ => makearglist (List.tl oplist) (List.tl arglist)
                                                                                 in
                                                                                    (pairopty oper typ)::rest
                                                                                 end
                    val arglist = makearglist ops argtypes
                    val (call_op, call_build) = emit_mk'' ("callExp", ll.Call (ty_to_llty retty
                                                                              , ll.Gid  (#gid f)
                                                                              , arglist
                                                                              ))
                    val b_ = seq_buildlets (builds@[call_build])
                in
                    (call_op,b_)
                end
       | A''.RecordExp {fields}                      => TODO ()
       | A''.IfExp {test, thn, els as NONE }         => 
    		let 
                val then_lbl = mk_label_name ll_env "then"
            	val merg_lbl = mk_label_name ll_env "merge"
                            
            	val isEmptyThen = if (#ty (#aug thn)) = Types.UNIT then (print("Return is empty \n"); true) else false
                val (test_oprnd, test_b) = cgE test
                val (then_oprnd, then_b) = if (not isEmptyThen) then cgE thn else (ll.Null, id_buildlet)

                val (test_op', build_icmp) =
		            emit_mk'  ("test"
			        	, ll.Icmp ( ll.Ne
				            , ll.I64
				        	, test_oprnd
				     		, ll.Const 0))
				
				val build_cbr =
                 	emit_term (ll.Cbr ( ll.Id test_op'
				   		, then_lbl
				   		, merg_lbl ))

				val uid = mk_uid ll_env "ifthen_result"
             	val llty = ty_to_llty ty
             	val b_res_alloca = if (not isEmptyThen) then emit_alloca (uid, llty) else (id_buildlet)
             	fun b_store_r r = if (not isEmptyThen) then build_store (llty, r , ll.Id uid) else (id_buildlet)

				val (res_val, b_load) =	build_load_op "res" (llty, ll.Id uid)
       			val b_load = if (not isEmptyThen) then b_load else (id_buildlet)

				val b_br_merge = emit_term (ll.Br merg_lbl)

                val b_ = seq_buildlets [
						b_res_alloca

					  , test_b
					  , build_icmp
					  , build_cbr
					  
					  , emit_lbl then_lbl
                   	  , then_b
                      , b_store_r then_oprnd
                      , b_br_merge
					  
					  , emit_lbl merg_lbl
                   	  , b_load ]
                                                            
            in
                (res_val, b_)
            end
                                                            
                                                        
       | A''.WhileExp {test, body}                  => 
	   		let
				val loop_guard_lbl = mk_label_name ll_env "loop_guard"
				val loop_body_lbl = mk_label_name ll_env "loop_body"
				val merg_lbl = mk_label_name ll_env "merge"

                val new_ctxt = { venv = #venv cg_ctxt
                               , tenv = #tenv cg_ctxt
                               , locals = #locals cg_ctxt
                               , lid = #lid cg_ctxt
                               , merg_lbl = SOME(merg_lbl)}

				val (test_oprnd, test_b) = cgE test
				val (body_oprnd, body_b) = cgExp ll_env new_ctxt body

				val (test_op', build_icmp) =
		            emit_mk'  ("test"
			        	, ll.Icmp ( ll.Ne
				            , ll.I64
				        	, test_oprnd
				     		, ll.Const 0))

				val build_cbr =
                 	emit_term (ll.Cbr ( ll.Id test_op'
				   		, loop_body_lbl
				   		, merg_lbl ))

				val build_start_br = 
					emit_term (ll.Br (loop_guard_lbl))

				val build_loop_guard_br = 
					emit_term (ll.Br (loop_guard_lbl))

				val b_ = seq_buildlets [
					build_start_br
				
				  , emit_lbl loop_guard_lbl
				  , test_b
				  , build_icmp
				  , build_cbr

				  , emit_lbl loop_body_lbl
				  , body_b
				  , build_loop_guard_br

				  , emit_lbl merg_lbl
				]
			in
				(ll.Null, b_)
			end
       | A''.BreakExp                                => 
	   		let
                val merg_lbl = (case (#merg_lbl cg_ctxt) of
                    SOME(lbl) => (lbl)
                  | NONE => (print("There is no label in the context"); raise CodegenFatal))

                val break_lbl = mk_label_name ll_env "break"

                val build_start_br = 
					emit_term (ll.Br (break_lbl))

				val build_break_br = 
					emit_term (ll.Br (merg_lbl))
                    
				val b_ = seq_buildlets [
                    build_start_br
                  , emit_lbl break_lbl
                    
				  , build_break_br
                  , emit_lbl merg_lbl
				]
			in
				(ll.Null, b_)
			end
       | A''.ForExp {var, escape, lo, hi, body, aug} => 
            let
                val init_lbl = mk_label_name ll_env "init"
                val loop_guard_lbl = mk_label_name ll_env "loop_guard"
		val loop_body_lbl = mk_label_name ll_env "loop_body"
                val merg_lbl = mk_label_name ll_env "merge"

                val new_ctxt = { venv = #venv cg_ctxt
                               , tenv = #tenv cg_ctxt
                               , locals = #locals cg_ctxt
                               , lid = #lid cg_ctxt
                               , merg_lbl = SOME(merg_lbl)}

                val (lo_oprnd, lo_b) = cgE lo
                val (hi_oprnd, hi_b) = cgE hi

                val (body_oprnd, body_b) = cgExp ll_env new_ctxt body

                val uid = var
             	val llty = ll.I64

                (* Allocate space for for-loop variable *)
                val build_alloca = emit_alloca (uid, llty)

                (* Initialise the for-loop variable to the lo-value *)
                val build_var = build_store(llty, lo_oprnd, ll.Id uid)

                (* Seperate load-ops for loading for-loop variable *)
                val (guard_var_val, build_load_var_guard) = build_load_op "var_guard" (llty, ll.Id uid)
                val (inc_var_val, build_load_var_inc) = build_load_op "var_inc" (llty, ll.Id uid)
								      
                (* Increment the for-loop variable *)
                val inc_insn = ll.Binop (ll.Add, ll.I64, inc_var_val, ll.Const 1)
                val (inc_uid, build_inc) = (emit_mk'  ("loop_increment", inc_insn))

                (* Stoe the incrementet value to the for-loop variable *)
                val build_store_inc = (build_store (llty, ll.Id inc_uid, ll.Id uid))

                (* Terminate the earlier block and start the new one*)
                val build_start_br = 
                    emit_term (ll.Br (init_lbl))

                (* Branch to loop guard*)
                val build_loop_br =
                    emit_term (ll.Br (loop_guard_lbl))

                val (test_op', build_icmp) =
		            emit_mk'  ("test"
			        	, ll.Icmp ( ll.Ne
				            , ll.I64
				        	, guard_var_val
				     		, hi_oprnd))
				
				val build_cbr =
                 	emit_term (ll.Cbr ( ll.Id test_op'
				   		, loop_body_lbl
				   		, merg_lbl ))

                val build_loop_guard_br =
                    emit_term (ll.Br (loop_guard_lbl))

                

                val b_ = seq_buildlets [
                    build_start_br
                  , build_alloca

                  , emit_lbl init_lbl
                  , lo_b
                  , hi_b
                  , build_var
                  , build_loop_guard_br

                  , emit_lbl loop_guard_lbl
                  , build_load_var_guard
                  , build_icmp
                  , build_cbr

                  , emit_lbl loop_body_lbl
				  , body_b
                  , build_load_var_inc
                  , build_inc
                  , build_store_inc
				  , build_loop_guard_br

                  , emit_lbl merg_lbl
                ]
            in
                (ll.Null, b_)
            end
       | A''.StringExp s                             => 
            let 
                val string_length = size s
                val name = s
                val gid = mk_gid ll_env ("string")
                val llgid =  gid

                val string_length_decl = (ll.I64, ll.GInt string_length)
                val string_array = (ll.Array (string_length, ll.I8), ll.GString)

                val gstring_decl = (ll.Array(string_length, ll.I8), ll.GString s)
                val gint_decl = (ll.I64, ll.GInt string_length)

                val gstruct_type = ll.Struct([ll.I64, ll.Array(string_length, ll.I8)])
                val gstruct_decl = (gstruct_type, ll.GStruct([gint_decl, gstring_decl]))

                val _ = add_gid_decl ll_env (llgid, gstruct_decl)

                val gstruct_type_pointer = ll.Ptr(gstruct_type)

                val bitcast_insn = (ll.Bitcast(gstruct_type_pointer, ll.Gid gid, ll.Ptr(ll.I8)))
                val (bitcast_op, build_bitcast) =
		            emit_mk' ("string_bitcast", bitcast_insn)

                val b_ = seq_buildlets [
                    build_bitcast
                ]
            in 
                (ll.Id bitcast_op, b_)
            end
       | A''.ArrayExp {size, init}                   =>
	 let
	     val intExp = #exp size
	 in
	     case (#exp size) of
		 A''.IntExp arraySize =>
		 (let
		     val init_lbl = mk_label_name ll_env "init"
		     val init_type = ty_to_llty (#ty (#aug init))
		     val build_start_br =  emit_term (ll.Br (init_lbl))
		     val (init_operand, build_init) = cgE init
		     val (init_pointer, build_gep_pointer) = emit_mk'' ("gep_init_pointer", ll.Gep (ll.I8, ll.Null, [ll.Const 0]))
		     val (gep_op, build_gep) = emit_mk'' ("gep_size_of_init", ll.Gep (init_type, ll.Null, [ll.Const 1]))
		     val (elem_size, build_ptrtoint) = emit_mk'' ("ptr_to_int", ll.Ptrtoint (init_type, gep_op, ll.I64))
					  
		     val call_inst = ll.Call (ll.Ptr ll.I8, ll.Gid (S.symbol "initArray"), [(ll.I64, ll.Const arraySize),(ll.I64, elem_size),(ll.Ptr ll.I8, init_pointer)])
		     val (call_operand, call_build) = emit_mk'' ("call", call_inst)
								
		     val b_ = seq_buildlets [build_start_br,
					     emit_lbl init_lbl,
					     build_init,
					     build_gep_pointer,
					     build_gep,
					     build_ptrtoint,
					     call_build]
					    
		 in
		     (call_operand, b_)
		 end)
	       | _ => raise CodegenFatal
	 end

       | A''.ErrorExp => raise CodegenFatal
  end

(*ordner funktioner*)
and fundecls ll_env (cg_ctxt:cg_ctxt) { name:S.symbol, args: A''.arg list, aug: A''.fun_aug, body: A''.exp } =
    let 
        fun getVarAug (arg:A''.arg) = (#aug arg)
        fun getNames (arg:A''.arg) = (#name arg)
        fun getTypes (varaugs:A''.var_aug) = (#ty varaugs)

        val locals = #locals aug
        val localname = (#1 (List.hd (#locals cg_ctxt)))

        val localsty_: ll.ty list = ll.Ptr(ll.Namedt localname) :: (map ty_to_llty ((#2 o ListPair.unzip)locals))
        val varauglist_  = map getVarAug args
        val argsty_ : ll.ty list = map ty_to_llty (map getTypes varauglist_)
        val ty_ : ll.ty list = localsty_ @ argsty_

        val fun_name   = mk_type_name ll_env (S.name name)
        val lid         = mk_uid ll_env "fundecls"
        val fun_struct = ll.Struct ty_

        val nylocal = (fun_name,fun_struct)
        (* emit the type declaration *)
        val _ = add_named_type ll_env (fun_name, fun_struct)

        (* builder for allocating the locals and args in the current block *)
        val build_alloca = emit_alloca (lid, ll.Namedt fun_name)


        (*ADD ALLE ARGUMENTER*)

        fun addArg current_venv (vararg:A''.arg) : venv = let
                                                    val varaug = #aug vararg
                                                    val argty = #ty varaug
                                                    val argoffset = #offset varaug
                                                    val nyVar = E.VarEntry{ty=argty,escape=(#escape vararg),level=List.length (#locals cg_ctxt)+1,offset=argoffset}
                                                    val returvenv = S.enter(current_venv,(#name vararg),nyVar)
                                                   in
                                                    returvenv
                                                   end
        
        fun addArgumenter current_venv vararglist : venv = let
                                                                val nyvenv = addArg current_venv (List.hd vararglist)
                                                                val resultvenv = case (List.tl vararglist) of [] => nyvenv
                                                                                                              |_ => addArgumenter nyvenv (List.tl vararglist)
                                                              in
                                                                resultvenv
                                                              end
        val temp_venv = case args of [] => #venv cg_ctxt |_ => addArgumenter (#venv cg_ctxt) args
        val temp_ctxt = {venv = temp_venv, tenv = #tenv cg_ctxt, locals = nylocal::(#locals cg_ctxt), lid = lid, merg_lbl = #merg_lbl cg_ctxt}

        (* codegen the rest of the expression *)
        val (oprnd, build_exp) = cgExp ll_env temp_ctxt body

        (* terminate the last (potentially the same as the first) block *)
        val (rt, tr) = case (( #ty o #aug) body ) of
                         Ty.UNIT => (ll.Void, ll.Ret (ll.Void, NONE))
                       | t       => (ty_to_llty t,
                                     ll.Ret (ty_to_llty t, SOME oprnd))


        (* make the terminator *)
        val build_ret = emit_term tr

        (* put things together to get the final CFG for the main *)

        val cfg = build_cfg_from_buildlets [  build_alloca
                                          , build_exp
                                          , build_ret ]

        (*generate uid list for arguments*)
        fun makeuid (argname:S.symbol) = mk_uid ll_env (S.name argname)
        val arguids = map makeuid (map getNames args)

        (* generate the function declaration *)
        val (fd:ll.fdecl) = { fty = ( argsty_, rt), param = fun_name::arguids, cfg = cfg}
        val venv = #venv cg_ctxt
        val nyFun = E.FunEntry{formals=map getTypes varauglist_(**), result=( #ty o #aug) body(**),gid=fun_name,sl_type=rt,level=List.length (#locals cg_ctxt)}
        val nyvenv = S.enter(venv,name,nyFun)(*opdater venv med symbolen og funEntry*)
        val ny_ctxt = {venv = nyvenv, tenv = #tenv cg_ctxt, locals = #locals cg_ctxt, lid = #lid cg_ctxt, merg_lbl = #merg_lbl cg_ctxt}
    in 
        (LLEnv.add_fun_decl ll_env (fun_name, fd); 
        (ny_ctxt,id_buildlet))
    end

(* code generation of declarations *)
and cgDecls ll_env (cg_ctxt:cg_ctxt) (aug_decls:OAbsyn.decl list) = 
    case aug_decls of [] => (cg_ctxt, id_buildlet) (*tom liste: returnerer ctxt og id_buildlet*)
    |_ =>
    let
      val (ny_ctxt,buildlets) = cgDecl ll_env cg_ctxt (List.hd aug_decls)
      val (ny_ctxt',buildlets') = cgDecls ll_env ny_ctxt (List.tl aug_decls)
      val b_ = seq_buildlets [buildlets,buildlets']
    in 
      (ny_ctxt',b_)
    end

and cgDecl ll_env (cg_ctxt:cg_ctxt) aug_decl = 
    case aug_decl
     of A''.VarDec { name, escape, aug as {ty, offset}, init } => 
                let
                    val (exp_op, exp_buildlet) = cgExp ll_env cg_ctxt init
                    val lltype = ty_to_llty ty
                (*opdaterer venv. *)
                val nyVar = E.VarEntry{ty=ty,escape=escape,level=0,offset=offset}
                val venv' = S.enter(#venv cg_ctxt, name, nyVar)
                
                val ny_ctxt = {venv = venv', tenv = #tenv cg_ctxt, locals = #locals cg_ctxt, lid = #lid cg_ctxt, merg_lbl = NONE}

                val locType = #1 (List.hd (#locals cg_ctxt)) (*tager typen (loc_name) fra context*)
                val lid = #lid cg_ctxt (*tager start adressen fra context*)
                val (gep_op', build_gep) = (*laver en gep til at lave en offset på addressen*)
                                emit_mk'' ll_env ("gep_vardec"
                                , ll.Gep ( ll.Namedt locType (*ll.Namedt kaldes på loc_name hver gang jeg skal bruge den*)
                                        , ll.Id lid
                                        , [ll.Const 0, ll.Const (offset+1)])
                                )
                val build_store = build_store ( lltype, exp_op, gep_op')
                (*buildletsne skal laves til en seq til sidst og returneres*)
                val b_= seq_buildlets [exp_buildlet, build_gep, build_store]

                in (*returner cg_txt og liste af insn*)
                (ny_ctxt, b_)
                end
      | A''.FunctionDec fns                                    => let
                                                                    fun funcs env (ctxt:cg_ctxt) functions =
                                                                    (case functions of [] => (ctxt,id_buildlet)
                                                                                        |_ => 
                                                                    let
                                                                        val (ny_ctxt,fun_buildlet) = fundecls env ctxt (List.hd functions)
                                                                        val (ny_ctxt',fun_buildlet') = funcs env ny_ctxt (List.tl functions)
                                                                        val b_ = seq_buildlets [fun_buildlet, fun_buildlet']
                                                                    in 
                                                                        (ny_ctxt',b_)
                                                                    end)
                                                                in
                                                                    funcs ll_env cg_ctxt fns
                                                                end
      | A''.TypeDec tds                                        =>
	let
	    fun addType (ctxt:cg_ctxt) (tyrec:A''.tydecldata) =
		let
		    val ttype = (#ty tyrec)
		    val lltype = ty_to_llty ttype
		    val tenv' = S.enter((#tenv ctxt), (#name tyrec), (ttype,lltype))
                    val ny_ctxt = {venv = #venv ctxt, tenv = tenv', locals = #locals ctxt, lid = #lid ctxt, merg_lbl = NONE}
		in
		    ny_ctxt
		end
		    
	    fun addTypes (ctxt:cg_ctxt) (typeList:A''.tydecldata list) = (case typeList of [] => (ctxt, id_buildlet)
											 | _ => addTypes (addType ctxt (hd typeList)) (tl typeList))
	in
	    addTypes cg_ctxt tds
	end
			    

(* cgVarLoc returns the location and a type for the variable to be
   used for subsequent load /saves --------------------------------------- *)

and cgVarLoc ll_env (cg_ctxt:cg_ctxt) (var_aug:A''.var) =
    let val var = #var var_aug
    in case var
	of A''.SimpleVar s => let
                            val venv = #venv cg_ctxt
                            val varEntry = (case S.look(venv,s) of SOME(E.VarEntry e) => e
                                                                   |_ => (print("Variable not found in environment"); raise CodegenFatal))
                            val vartype = ty_to_llty (#ty varEntry)
                            val offset = #offset varEntry
                            val locType = #1 (List.hd (#locals cg_ctxt)) (*tager typen (loc_name) fra context*)
                            val lid = #lid cg_ctxt (*tager start adressen fra context*)
                            val (gep_op', build_gep) = (*laver en gep til at lave en offset på addressen*)
                                emit_mk'' ll_env ("gep_vardec"
                                , ll.Gep ( ll.Namedt locType (*ll.Namedt kaldes på loc_name hver gang jeg skal bruge den*)
                                        , ll.Id lid
                                        , [ll.Const 0, ll.Const (offset+1)])
                                )
                            in
                                (gep_op', vartype, build_gep)
                            end
	 | A''.FieldVar (var, s) => TODO ()
	 | A''.SubscriptVar (var, exp) => TODO ()
    end

(* code generation of variables ---------------------------------------------- *)
and cgVar ll_env  (cg_ctxt:cg_ctxt) var_aug =
    let val loc = cgVarLoc ll_env cg_ctxt var_aug (* call cgVarLoc to get the location of the variable *)
	val var = #var var_aug
	val ty  =  (#ty o #aug) var_aug
    val llty = ty_to_llty ty
    val (load_oper, load_build) = emit_mk'' ll_env ("cgVarLoad", ll.Load(
                                                                    llty
                                                                   , #1 loc))

                                                                    
    val b_ = seq_buildlets [#3 loc,load_build]
    in 
        (load_oper, b_)
    end

(* coge generation for main -------------------------------------------------- *)
fun cgMain (ll_env: LLEnv.ll_env)
	   { exp:A''.exp, locals: (int * Ty.ty) list } =
  let val ty_: ll.ty list = (* we put I1 as a placeholder *)
	  ll.I1 :: (map ty_to_llty ((#2 o ListPair.unzip) locals))

      val locs_struct = ll.Struct ty_
      val locs_name   = LLEnv.mk_type_name ll_env "tigermain"
      val lid         = LLEnv.mk_uid ll_env "locals"

      val cg_ctxt = {
          venv = E.baseVenv
        , tenv = E.baseTenv
        , locals = [ (locs_name, locs_struct) ]
        , lid = lid
        , merg_lbl = NONE
      }

      (* emit the type declaration *)
      val _ = LLEnv.add_named_type ll_env (locs_name, locs_struct)

      (* builder for allocating the locals in the current block *)
      val build_alloca = emit_alloca (lid, ll.Namedt locs_name)

      (* codegen the rest of the expression *)
      val (oprnd, build_exp) = cgExp ll_env cg_ctxt exp

      (* terminate the last (potentially the same as the first) block *)
      val (rt, tr) = case (( #ty o #aug) exp ) of
                         Ty.UNIT => (ll.Void, ll.Ret (ll.Void, NONE))
                       | t       => (ty_to_llty t,
                                     ll.Ret (ty_to_llty t, SOME oprnd))


      (* make the terminator *)
      val build_ret = emit_term tr

      (* put things together to get the final CFG for the main *)

      val cfg = build_cfg_from_buildlets [  build_alloca
                                          , build_exp
                                          , build_ret ]

      (* generate the function declaration *)
      val (fd:ll.fdecl) = { fty = ( [], rt), param = [], cfg = cfg}

  in
      (* emit the function to the generated LL environment *)
      LLEnv.add_fun_decl ll_env (S.symbol "tigermain", fd)
  end


(* ------------------------------------------------------------------ *)
(* The rest of the code below is does not require any changes         *)
(* though you are free to change it if needed e.g., for debugging     *)

fun newline s = s^"\n"

val runtime_fns =
    let val fns = [ "i8* @allocRecord(i64)"   (* runtime functions *)
		  , "i8* @initArray (i64, i64, i8*)"
		  , "i64 @stringEqual (i8*, i8*)"
		  , "i64 @stringNotEq (i8*, i8*)"
		  , "i64 @stringLess (i8*, i8*)"
		  , "i64 @stringLessEq (i8*, i8*)"
		  , "i64 @stringGreater (i8*, i8*)"
		  , "i64 @stringGreaterEq (i8*, i8*)"


		  , "void @print    (i8*, i8*)"   (* user visible functions; note SL argument *)
		  , "void @flush    (i8*)"
		  , "i8*  @getChar  (i8*)"
		  , "i64  @ord      (i8*, i8*)"
		  , "i8*  @chr      (i8*, i64)"
		  , "i64  @size     (i8*, i8*)"
		  , "i8*  @substring(i8*, i8*, i64, i64)"
		  , "i8*  @concat   (i8*, i8*, i8*)"
		  , "i64  @not      (i8*, i64)"
		  , "void @exit     (i8*, i64)"
		  ]
	fun mkDeclare s = "declare " ^ s ^ "\n"
    in String.concat (map mkDeclare fns)
    end

val target_triple =
  case LocalOS.os of
     LocalOS.Darwin => "target triple = \"x86_64-apple-macosx10.13.0\""
   | LocalOS.Linux  => "target triple = \"x86_64-pc-linux-gnu\""


fun codegen_prog (aug_offset as { exp:A''.exp, locals: (int * Ty.ty) list }) =
  let val ll_env  = LLEnv.init_ll_env ()
      val _    = cgMain ll_env aug_offset
      val prog = LLEnv.prog_of_env ll_env
  in prog
  end

(* a wrapper around codegen_prog *)
fun codegen_ll aug_offset =
  let val prog = codegen_prog aug_offset
      val s = ll.string_of_prog prog
      val s' = s ^ "\n" ^ runtime_fns ^ (newline target_triple)
  in s'
  end

end (* struct LLCodegen *)

(*SPØRGSMÅL TIL BENJAMIN:

- cgDecls: er det ok vi ikke tager hensyn til tom liste?



--> parametrer til funktioner gemmes som uid i param til sidst
--> fix if-then-else med unit returtype
*)
