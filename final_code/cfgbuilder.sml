(* Aarhus University, Compilation 2016  *)
(* DO NOT DISTRIBUTE                    *)
(* DO NOT CHANGE THIS FILE              *)

(* CfgBuilder - a helper file for constructing Control Flow Graphs 
            for IR translation *)

structure CfgBuilder = 
struct

(* a record for representing the construction of the CFG  *)
type cfg_builder =
     { bb_rev      : ( ll.lbl * ll.block) list
     (* (reversed) list of all but the first basic blocks *)
					  
     , insn_rev    : ( ll.uid option * ll.insn ) list
     (* (reversed) list of the instructions that have not yet been allocated 
      a basic block *)
						 
     , bb_first    : ll.block option
     (* first basic block, if any*)
			      
     , lbl_current : ll.lbl option
     (* label of the current basic block*)
			    
     , allocas_rev : ( ll.uid * ll.ty) list
     (* reserved alloca's *)

     }


	 
(* this exception is thrown if the module is used incorrectly *)
exception CFGConstructionError of string

(* this exception should never be thrown *)				      
exception Impossible


(* Aauxiliary functions for accessing and manipulating the CFG -- *)


(* add a reserved alloca *)
fun add_alloca (c:cfg_builder) (uid, ty) =
    { bb_rev      = #bb_rev c
    , insn_rev    = #insn_rev c
    , bb_first    = #bb_first c
    , lbl_current = #lbl_current c
    , allocas_rev = (uid, ty)::(#allocas_rev c) 
  }


(* add an instruction to the current list of instructions *)	
fun add_insn (c:cfg_builder) (insn_: ll.uid option * ll.insn) =
  { bb_rev      = #bb_rev c
  , insn_rev    = insn_::(#insn_rev c)
  , bb_first    = #bb_first c
  , lbl_current = #lbl_current c
  , allocas_rev = #allocas_rev c
  }

(* terminate the current block *) 
fun term_block (cfg_:cfg_builder) (tr:ll.terminator) =
  let val (bb:ll.block) = { insns = List.rev (#insn_rev cfg_)
			  , terminator = tr }
  in case #bb_first cfg_ 
       of NONE => 
	  { bb_rev = #bb_rev cfg_
	  , insn_rev = []
	  , bb_first = SOME bb
	  , lbl_current = #lbl_current cfg_
	  , allocas_rev = #allocas_rev cfg_
	  }
	| SOME _ =>
	  ( case #lbl_current cfg_ of 
	       SOME lb => 
	       { bb_rev =   (lb, bb):: (#bb_rev cfg_)
	       , insn_rev = []
	       , bb_first = #bb_first cfg_
	       , lbl_current = #lbl_current cfg_
	       , allocas_rev = #allocas_rev cfg_
	       } 
	       | _ => raise Impossible
	  )
  end

(* set label for the current block *)
fun set_label (c:cfg_builder) (lbl: ll.lbl) =
  { bb_rev      = #bb_rev c
  , insn_rev    = #insn_rev c
  , bb_first    = #bb_first c
  , lbl_current = SOME lbl
  , allocas_rev = #allocas_rev c
  }

(* produce a new CFG environment *)      
fun new_env () =
  {
    bb_rev      = []
  , insn_rev    = []
  , bb_first    = NONE
  , lbl_current = NONE
  , allocas_rev = []
  }

(* get a CFG from the builder *)      
fun get_cfg (cfg_builder:cfg_builder) =
    case #bb_first cfg_builder
     of SOME (b as {insns, terminator})  =>
	let val allocas = List.map (fn (uid, ty) =>
				       (SOME uid, ll.Alloca ty))
				   (#allocas_rev cfg_builder)
	    val b' = { insns = allocas @ insns, terminator = terminator}
	in (b' , List.rev (#bb_rev cfg_builder))
	end
  | _ => raise CFGConstructionError "basic block is not available" 
      
end
