	.text
	.globl	Tempty3
Tempty3:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	subq	$8, %rsp
	movq	%rsp, -16(%rbp)
	movq	$1, %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	
	.text
	.globl	tigermain
tigermain:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$48, %rsp
	subq	$16, %rsp
	movq	%rsp, -8(%rbp)
	movq	-8(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -16(%rbp)
	movq	-16(%rbp), %r9
	movq	$5, %r10
	movq	%r10, (%r9)
	movq	-8(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -24(%rbp)
	movq	-24(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -32(%rbp)
	leaq	Tempty3(%rip), %r10
	callq	*%r10
	movq	-32(%rbp), %rbx
	movq	-40(%rbp), %rcx
	addq	%rbx, %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	