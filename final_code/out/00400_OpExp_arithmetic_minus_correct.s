	.text
	.globl	tigermain
tigermain:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	subq	$8, %rsp
	movq	%rsp, -8(%rbp)
	movq	$2, %rcx
	movq	$2, %rbx
	subq	%rbx, %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	