	.text
	.globl	tigermain
tigermain:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$120, %rsp
	subq	$8, %rsp
	movq	%rsp, -8(%rbp)
	subq	$16, %rsp
	movq	%rsp, -16(%rbp)
	movq	-16(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -24(%rbp)
	movq	-24(%rbp), %r9
	movq	$0, %r10
	movq	%r10, (%r9)
	movq	$3, %r9
	movq	$3, %r10
	cmpq	%r10, %r9
	movq	$0, -32(%rbp)
	sete	-32(%rbp)
	movq	-32(%rbp), %r9
	movq	%r9, -40(%rbp)
	movq	-40(%rbp), %r9
	movq	$0, %r10
	cmpq	%r10, %r9
	movq	$0, -48(%rbp)
	setne	-48(%rbp)
	movq	-48(%rbp), %r9
	movq	$0, %r10
	cmpq	%r9, %r10
	jne	Lthen3
	jmp	Lelse4
	.text
Lthen3:
	movq	-16(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -56(%rbp)
	movq	-56(%rbp), %r9
	movq	$1, %r10
	movq	%r10, (%r9)
	movq	-16(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -64(%rbp)
	movq	-64(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -72(%rbp)
	movq	-8(%rbp), %r9
	movq	-72(%rbp), %r10
	movq	%r10, (%r9)
	jmp	Lmerge5
	.text
Lelse4:
	movq	-16(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -80(%rbp)
	movq	-80(%rbp), %r9
	movq	$2, %r10
	movq	%r10, (%r9)
	movq	-16(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -88(%rbp)
	movq	-88(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -96(%rbp)
	movq	-8(%rbp), %r9
	movq	-96(%rbp), %r10
	movq	%r10, (%r9)
	jmp	Lmerge5
	.text
Lmerge5:
	movq	-8(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -104(%rbp)
	movq	-16(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -112(%rbp)
	movq	-112(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	