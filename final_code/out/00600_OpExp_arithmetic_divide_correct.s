	.text
	.globl	tigermain
tigermain:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	subq	$8, %rsp
	movq	%rsp, -8(%rbp)
	movq	$0, %rdx
	movq	$2, %rax
	movq	$2, %rcx
	idivq	%rcx
	movq	%rax, %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	