	.text
	.globl	tigermain
tigermain:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$32, %rsp
	subq	$16, %rsp
	movq	%rsp, -8(%rbp)
	movq	-8(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -16(%rbp)
	movq	-16(%rbp), %r9
	movq	$10, %r10
	movq	%r10, (%r9)
	movq	-8(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -24(%rbp)
	movq	-24(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	