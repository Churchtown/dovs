	.text
	.globl	tigermain
tigermain:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	subq	$8, %rsp
	movq	%rsp, -8(%rbp)
	movq	$2, %rdi
	movq	$2, %rsi
	leaq	exponent(%rip), %r10
	callq	*%r10
	movq	-16(%rbp), %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	