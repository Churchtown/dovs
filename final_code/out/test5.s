	.text
	.globl	tigermain
tigermain:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$104, %rsp
	subq	$8, %rsp
	movq	%rsp, -8(%rbp)
	subq	$24, %rsp
	movq	%rsp, -16(%rbp)
	movq	-16(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -24(%rbp)
	movq	-24(%rbp), %r9
	movq	$0, %r10
	movq	%r10, (%r9)
	jmp	Linit3
	.text
Linit3:
	movq	-8(%rbp), %r9
	movq	$0, %r10
	movq	%r10, (%r9)
	jmp	Lloop_guard4
	.text
Lloop_guard4:
	movq	-8(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %r9
	movq	$4, %r10
	cmpq	%r10, %r9
	movq	$0, -40(%rbp)
	setne	-40(%rbp)
	movq	-40(%rbp), %r9
	movq	$0, %r10
	cmpq	%r9, %r10
	jne	Lloop_body5
	jmp	Lmerge6
	.text
Lloop_body5:
	movq	-16(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -48(%rbp)
	movq	-48(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rbx
	movq	$1, %rcx
	addq	%rbx, %rcx
	movq	%rcx, -64(%rbp)
	movq	-16(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -72(%rbp)
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r10
	movq	%r10, (%r9)
	movq	-8(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rbx
	movq	$1, %rcx
	addq	%rbx, %rcx
	movq	%rcx, -88(%rbp)
	movq	-8(%rbp), %r9
	movq	-88(%rbp), %r10
	movq	%r10, (%r9)
	jmp	Lloop_guard4
	.text
Lmerge6:
	movq	-16(%rbp), %r9
	addq	$1, %r9
	movq	%r9, -96(%rbp)
	movq	-96(%rbp), %r9
	movq	(%r9), %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	