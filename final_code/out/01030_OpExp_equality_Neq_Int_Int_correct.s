	.text
	.globl	tigermain
tigermain:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$24, %rsp
	subq	$8, %rsp
	movq	%rsp, -8(%rbp)
	movq	$5, %r9
	movq	$3, %r10
	cmpq	%r10, %r9
	movq	$0, -16(%rbp)
	setne	-16(%rbp)
	movq	-16(%rbp), %r9
	movq	%r9, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rbp, %rsp
	popq	%rbp
	retq	