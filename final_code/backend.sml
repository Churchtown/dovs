(* AU Compilation 2016                  *)
(* DO NOT DISTRIBUTE                    *)


(* LLVM-- to x86 backend skeleton file  *)
(* Based on LLVMLite X86 backend@UPenn  *)




(* ll ir compilation -------------------------------------------------------- *)


signature X86BACKEND =
sig
  val compile_prog: ll.prog -> X86.prog
end


structure X86Backend :> X86BACKEND =
struct


structure S = Symbol

open X86
open ll


exception NotImplemented
exception ShouldNotHappen

fun TODO() = (print "hit a todo"; raise NotImplemented)

(* Helpers ------------------------------------------------------------------ *)


(* Platform-specific generation of symbols *)

val mangle =
 fn s =>
    (case LocalOS.os of
	LocalOS.Linux =>   (Symbol.name s)
     |  LocalOS.Darwin => "_" ^ (Symbol.name s))


(* Map ll comparison operations to X86 condition codes *)
fun compile_cnd (c:ll.cnd) : X86.cnd =
 case c of
    ll.Eq  => X86.Eq
  | ll.Ne  => X86.Neq
  | ll.Slt => X86.Lt
  | ll.Sle => X86.Le
  | ll.Sgt => X86.Gt
  | ll.Sge => X86.Ge



(* locals and layout -------------------------------------------------------- *)
(* We call the datastructure that maps each %uid to its stack slot a
   'stack layout'.  A stack layout maps a uid to an X86 operand for
   accessing its contents.  For this compilation strategy, the operand
   is always an offset from rbp (in bytes) that represents a storage slot in
   the stack.
*)

type layout = (ll.uid * X86.operand) list

(* A context contains the global type declarations (needed for getelementptr
   calculations) and a stack layout. *)
type ctxt = { tdecls : (ll.tid * ll.ty) list
            , layout : layout
            }

(* useful for looking up items in tdecls or layouts *)
(* m er listen, x er den variabel vi gerne vil finde i listen *)
fun lookup m x = let val (_, r) = (valOf (List.find (fn (y, _) => x = y) m))
                 in r
                 end


(* compiling operands  ------------------------------------------------------ *)

(* LLVM IR instructions support several kinds of operands.

   LL local %uids live in stack slots, whereas global ids live at
   global addresses that must be computed from a label.  Constants are
   immediately available, and the operand Null is the 64-bit 0 value.

     NOTE: two important facts about global identifiers:

     (1) You should use (mangle gid) to obtain a string
     suitable for naming a global label on your platform (macOS expects
     "_main" while linux expects "main").

     (2) 64-bit assembly labels are not allowed as immediate operands.
     That is, the X86 code: movq _gid %rax which looks like it should
     put the address denoted by _gid into %rax is not allowed.
     Instead, you need to compute an %rip-relative address using the
     leaq instruction: leaq _gid(%rip).

   One strategy for compiling instruction operands is to use a
   designated register (or registers) for holding the values being
   manipulated by the LLVM IR instruction. You might find it useful to
   implement the following helper function, whose job is to generate
   the X86 instruction that moves an LLVM operand into a designated
   destination (usually a register).
*)


fun compile_operand {tdecls, layout} dest (oper:ll.operand) : ins = 
    let
	val xoper = (case oper of ll.Id(e) => let
			  val layout_op = lookup layout e
		      in
			  (Movq,[layout_op,dest])
		      end
                                 |ll.Gid(e) => let
				     val mangledgid = mangle e
				 in
				     (Leaq,[Ind3 (Lbl mangledgid, Rip),dest])
				 end
                                 |ll.Const(e) => let
				     val layout_op = Imm (Lit e)
				 in
				     (Movq,[layout_op,dest])
				 end
                                 |ll.Null => let
				     val layout_op = Imm (Lit 0)
				 in
				     (Movq,[layout_op,dest])
				 end)
    in
	xoper
    end


(* compiling call  ---------------------------------------------------------- *)

(* You will probably find it helpful to implement a helper function that
   generates code for the LLVM IR call instruction.

   The code you generate should follow the x64 System V AMD64 ABI
   calling conventions, which places the first six 64-bit (or smaller)
   values in registers and pushes the rest onto the stack.  Note that,
   since all LLVM IR operands are 64-bit values, the first six
   operands will always be placed in registers.  (See the notes about
   compiling fdecl below.)

   [ NOTE: It is the caller's responsibility to clean up arguments
   pushed onto the stack, so you must free the stack space after the
   call returns. ]

   [ NOTE: Don't forget to preserve caller-save registers (only if
   needed). ]
*)





(* compiling getelementptr (gep)  ------------------------------------------- *)

(* The getelementptr instruction computes an address by indexing into
   a datastructure, following a path of offsets.  It computes the
   address based on the size of the data, which is dictated by the
   data's type.

   To compile getelmentptr, you must generate x86 code that performs
   the appropriate arithemetic calculations.
*)





(* Function size_ty maps an LLVMlite type to a size in bytes.
   (needed for getelementptr)

   - the size of a struct is the sum of the sizes of each component
   - the size of an array of t's with n elements is n * the size of t
   - all pointers, I1, and I64 are 8 bytes
   - the size of a named type is the size of its definition

   - Void, i8, and functions have undefined sizes according to LLVMlite
     your function should simply return 0
*)

fun size_ty tdecls t : int = case t of ll.Void => 0
				     | ll.I1 => 8
				     | ll.I8 => 0
				     | ll.I64 => 8
				     | ll.Ptr(_) => 8
				     | ll.Struct(tlist) => sum_list_ty tdecls tlist 
				     | ll.Array(size,ty) => size * (size_ty tdecls ty)
				     | ll.Fun(_) => 0
				     | ll.Namedt(tid) => size_ty tdecls (lookup tdecls tid)
and sum_list_ty tdecls tlist : int = case tlist of [] => 0
						 | _ => (size_ty tdecls (hd tlist)) + (sum_list_ty tdecls (tl tlist))





(* Function compile_gep generates code that computes a pointer value.

   1. op must be of pointer type: t*

   2. the value of op is the base address of the calculation

   3. the first index in the path is treated as the index into an array
     of elements of type t located at the base address

   4. subsequent indices are interpreted according to the type t:

     - if t is a struct, the index must be a constant n and it
       picks out the n'th element of the struct. [ NOTE: the offset
       within the struct of the n'th element is determined by the
       sizes of the types of the previous elements ]

     - if t is an array, the index can be any operand, and its
       value determines the offset within the array.

     - if t is any other type, the path is invalid

   5. if the index is valid, the remainder of the path is computed as
      in (4), but relative to the type f the sub-element picked out
      by the path so far
*)

fun compile_gep (ctxt as {tdecls, layout})
                (oper : ll.ty * ll.operand)
                (path: ll.operand list) : ins list = TODO ()




(* compiling instructions  -------------------------------------------------- *)

(* The result of compiling a single LLVM instruction might be many x86
   instructions.  We have not determined the structure of this code
   for you. Some of the instructions require only a couple assembly
   instructions, while others require more.  We have suggested that
   you need at least compile_operand, compile_call, and compile_gep
   helpers; you may introduce more as you see fit.

   Here are a few notes:

   - Icmp:  the Set instruction may be of use.  Depending on how you
     compile Cbr, you may want to ensure that the value produced by
     Icmp is exactly 0 or 1.

   - Load & Store: these need to dereference the pointers. Const and
     Null operands aren't valid pointers.  Don't forget to mmangle
     the global identifier.

   - Alloca: needs to return a pointer into the stack

   - Bitcast, Ptrtoint, Zext: do nothing interesting at the assembly level
*)

fun compile_insn (ctxt as {tdecls, layout}) (uid_opt, i) : X86.ins list = (* Denne funktion skal returnere en tupel af X86.opcode, X86.opreand list *) (* layout har typen (ll.uid * X86.operand) list *)
    ((* print "kom ind i insn \n"; *)
     case i of ll.Binop(bop, ty, l_operand, r_operand) =>
	       let
		   (*val _ =  (print "kom ind i add \n") *)
		   val loper_x86 = (case l_operand of ll.Id(e) => lookup layout e
						     |ll.Gid(e) => lookup layout e
						     |ll.Const(e) => Imm (Lit e)
						     |_ => raise ShouldNotHappen)
				       
		   val roper_x86 = (case r_operand of ll.Id(e) => lookup layout e
						     |ll.Gid(e) => lookup layout e
						     |ll.Const(e) => Imm (Lit e)
						     |_ => raise ShouldNotHappen)
				       
		   val result = case bop of
				    ll.Add => [
								(Movq, [loper_x86, Reg(Rbx)])
							  , (Movq, [roper_x86, Reg(Rcx)])
							  , (Addq, [Reg(Rbx), Reg(Rcx)])
							]
				  | ll.Sub => [
								(Movq, [loper_x86, Reg(Rcx)])
							  , (Movq, [roper_x86, Reg(Rbx)])
							  , (Subq, [Reg(Rbx), Reg(Rcx)])
							]
				  | ll.Mul => [
								(Movq, [loper_x86, Reg(Rbx)])
							  , (Movq, [roper_x86, Reg(Rcx)])
							  , (Imulq, [Reg(Rbx), Reg(Rcx)])
							]
				  | ll.SDiv => [
								(Movq, [Imm (Lit 0), Reg(Rdx)])
							  , (Movq, [loper_x86, Reg(Rax)])
							  , (Movq, [roper_x86, Reg(Rcx)])
							  , (Idivq, [Reg(Rcx)])
							  , (Movq, [Reg(Rax), Reg(Rcx)])
							]
				  | ll.Shl => TODO ()
				  | ll.Lshr => TODO ()
				  | ll.Ashr => TODO ()
				  | ll.And => [
								(Movq, [loper_x86, Reg(Rbx)])
							  , (Movq, [roper_x86, Reg(Rcx)])
							  , (Andq, [Reg(Rbx), Reg(Rcx)])
							]
				  | ll.Or => [
								(Movq, [loper_x86, Reg(Rbx)])
							  , (Movq, [roper_x86, Reg(Rcx)])
							  , (Orq, [Reg(Rbx), Reg(Rcx)])
							]
				  | ll.Xor => [
								(Movq, [loper_x86, Reg(Rbx)])
							  , (Movq, [roper_x86, Reg(Rcx)])
							  , (Xorq, [Reg(Rbx), Reg(Rcx)])
							]
					      
		   val move_reg = (case uid_opt of SOME(e)=> lookup layout e
						  |NONE => Reg R08 )
		   val moveins = [(Movq,[Asm.~%Rcx, move_reg])]
	       in
		   result @ moveins
	       end
	     | ll.Alloca(ty) =>
	       let
		   (* val _ = (print "før size \n") *)
		   val alloca_size = size_ty tdecls ty (* Calculate allocation size *)
		   (* val _ = (print "efter size \n") *)  (* Save the sp, which is going to be the pointer to the allocation *)
		   val inc_sp = (Subq, [Imm(Lit alloca_size), Reg Rsp]) (* Increment sp by the allocation size *)
		   val return_variable = case uid_opt of SOME(uid) => lookup layout uid (* Locaton of return variable *)
						       | NONE => raise ShouldNotHappen
		   val move_pointer_to_result = (Movq, [Reg Rsp, return_variable]) (* Move old sp to return variable*)
	       in
		   [inc_sp, move_pointer_to_result]
	       end
    (*1. Save the old stack pointer
      2.  Increment the stack pointer by the size of ty
      3.   Put the value of the old stack pointer into uid so we return the reference to the allocation *)
    | ll.Load(ty,operand) => 
      let
	  val oper = compile_operand ctxt (Reg R09) operand (* The content of operand goes to register R09 *)
	  val uid = case uid_opt of SOME(uid) => lookup layout uid 
				  | NONE => raise ShouldNotHappen
      in
	  [oper, (Movq, [Ind2 R09, Reg Rax]), (Movq, [Reg Rax, uid])] (* move contents of R09 (the operand) to the wanted location *)
      end
    | ll.Store(ty, l_operand, r_operand) => 
      let
	  val l_oper  = case l_operand of ll.Const(e) => Imm (Lit e)
					| ll.Gid(e) => lookup layout e
					| ll.Id(e) => lookup layout e
					| ll.Null => Imm (Lit 0)
	  val r_oper = case r_operand of ll.Const(e) => Imm (Lit e)
					| ll.Gid(e) => lookup layout e
					| ll.Id(e) => lookup layout e
					| ll.Null => Imm (Lit 0)
      in
	  [(Movq, [r_oper, Reg R09]),(Movq, [l_oper, Reg R10]),(Movq, [Reg R10, Ind2 R09])]
      end
    | ll.Icmp(cnd, ty, l_operand, r_operand) => 
      let
	  val l_oper_ins = compile_operand ctxt (Reg R09) l_operand
	  val r_oper_ins = compile_operand ctxt (Reg R10) r_operand
	  val uid = case uid_opt of SOME(uid) => lookup layout uid
				  | NONE => raise ShouldNotHappen
	  val x86_cnd = compile_cnd cnd
	  val result = [l_oper_ins, r_oper_ins, (Cmpq, [Reg R10, Reg R09]), (Movq, [Imm(Lit 0), uid]), (Set x86_cnd, [uid])]
      in
	  result
      end
    | ll.Call(ty, operand, tyoperandlist) => 
      let
	                val x86_operand = [(compile_operand ctxt (Reg R10) operand)]
			fun insertArgsOnStack i (tyoperandlist : (ll.ty * ll.operand) list) = 
			case tyoperandlist of 
					[] => []
				  |  _ => 
						let
							val size = size_ty tdecls (#1 (hd tyoperandlist))
							val operand = (#2 (hd tyoperandlist))
							val insn = compile_operand ctxt (Reg Rsp) operand
							val inc_sp = (Subq, [Imm(Lit size), Reg Rsp]) (* Increment sp by the allocation size *)
						in
							[insn, inc_sp] @ (insertArgsOnStack (i+1) (tl tyoperandlist))
						end

			fun insertArgs i (tyoperandlist : (ll.ty * ll.operand) list) =
				case tyoperandlist of 
					[] => []
				  |  _ =>

					(case i of
						0 => [(compile_operand ctxt (Reg Rdi) (#2 (hd tyoperandlist)))] @ (insertArgs (i+1) (tl tyoperandlist))
	  			 	  | 1 => [(compile_operand ctxt (Reg Rsi) (#2 (hd tyoperandlist)))] @ (insertArgs (i+1) (tl tyoperandlist))
				 	  | 2 => [(compile_operand ctxt (Reg Rdx) (#2 (hd tyoperandlist)))] @ (insertArgs (i+1) (tl tyoperandlist))
				  	  | 3 => [(compile_operand ctxt (Reg Rcx) (#2 (hd tyoperandlist)))] @ (insertArgs (i+1) (tl tyoperandlist))
				 	  | 4 => [(compile_operand ctxt (Reg R08) (#2 (hd tyoperandlist)))] @ (insertArgs (i+1) (tl tyoperandlist))
				  	  | 5 => [(compile_operand ctxt (Reg R09) (#2 (hd tyoperandlist)))] @ (insertArgs (i+1) (tl tyoperandlist))
				  	  | _ => insertArgsOnStack i tyoperandlist)
		in
			(insertArgs 0 tyoperandlist) @ x86_operand @ [(Callq, [Reg R10])] (* we put the location of the function in R10 and call it *)
		end
			 (* tyoperandlist = liste af argumenter, de første 6 skal puttes i de rigtige register, resten på stacken (se slide "Compilers-17-LL_to_x86")*)
    | ll.Bitcast(ty1, operand, ty2) => 
      let
	  val oper_ins = compile_operand ctxt (Reg R09) operand
	  val uid = case uid_opt of SOME(uid) => lookup layout uid
				  | NONE  => raise ShouldNotHappen
      in     
	  [oper_ins,(Movq, [Reg R09, uid])]
      end
    | ll.Gep(ty, operand1, operandlist) =>
      let
	  val oper_result = compile_operand ctxt (Reg R09) operand1
	  val destination = case uid_opt of SOME(uid) => lookup layout uid
					  | NONE => raise ShouldNotHappen
    val offset = (case (List.hd (List.tl operandlist)) of ll.Const(i) => i
                                                         |_ => raise ShouldNotHappen)
    val dest_oper = (Addq, [Imm (Lit offset), Reg R09])
      in
	      [oper_result,dest_oper, (Movq, [Reg R09, destination])]
      end
    | ll.Zext(ty1, operand, ty2) =>
      let
	  val oper_ins = compile_operand ctxt (Reg R09) operand
	  val uid = case uid_opt of SOME(uid) => lookup layout uid
				  | NONE  => raise ShouldNotHappen
      in     
	  [oper_ins,(Movq, [Reg R09, uid])]
      end
    | ll.Ptrtoint(ty1, operand, ty2) => 
      let
	  val oper_ins = compile_operand ctxt (Reg R09) operand
	  val uid = case uid_opt of SOME(uid) => lookup layout uid
				  | NONE  => raise ShouldNotHappen
      in     
	  [oper_ins,(Movq, [Reg R09, uid])]
      end)
	
						      
(* compiling terminators  --------------------------------------------------- *)

(* Compile block terminators is not too difficult:

   - Ret should properly exit the function: freeing stack space,
     restoring the value of %rbp, and putting the return value (if
     any) in %rax.

   - Br should jump

   - Cbr branch should treat its operand as a boolean conditional
*)

fun compile_terminator ctxt t : ins list =
    case t of ll.Ret(ty, optional_operand) => 
              (let
		  (* val _ = print "laver en ret" *)
            val move_ins = (case optional_operand of SOME(e)=> compile_operand ctxt (Reg Rax) e
                                                  |NONE => compile_operand ctxt (Reg Rax) ll.Null)
            val epi_move = (Movq,[(Asm.~%Rbp), (Asm.~%Rsp)])
            val epi_pop = (Popq, [(Asm.~%Rbp)])
            val epi_ret = (Retq,[])
          in
            [move_ins, epi_move, epi_pop, epi_ret]
          end)
	    | ll.Br(label) => let
		(* val _ = print "laver en br" *)
	    in
			[(Jmp,[Imm (Lbl ( S.name label))])]
	    end
	    | ll.Cbr(operand, lbl1, lbl2) => let
		(* val _ = print "laver en cbr" *)
                       val oper_ins = compile_operand ctxt (Reg R09) operand
                       val cnd = Neq
		       val move_one = (Movq, [Imm (Lit 0), Reg R10])
                       val comp = (Cmpq, [Reg R09, Reg R10])
                       val condjump = (J cnd,[Imm (Lbl ( S.name lbl1))])
                       val normjump = (Jmp,[Imm (Lbl ( S.name lbl2))])
                    in
                      [oper_ins,move_one,comp,condjump,normjump]
                    end



(* compiling blocks --------------------------------------------------------- *)

(* We have left this helper function here for you to complete. *)
fun compile_block ctxt {insns, terminator} : ins list = 
    let
      fun comp_insns ctxt inslist : ins list = let
                                            val curr_list = compile_insn ctxt (List.hd inslist)
                                            val result = (case List.tl inslist of [] => curr_list
                                                                                  |_ => curr_list @ comp_insns ctxt (List.tl inslist))
                                            (* val _= print "length of list \n" *)
                                          in
                                            result
                                          end
      val list_from_insns = comp_insns ctxt insns
      val list_from_term = compile_terminator ctxt terminator
    in
      list_from_insns @ list_from_term
    end

fun compile_lbl_block ctxt (lbl, blk) : elem =
  Asm.text (S.name lbl) (compile_block ctxt blk)


(* compile_fdecl ------------------------------------------------------------ *)


(* This helper function computes the location of the nth incoming
   function argument: either in a register or relative to %rbp,
   according to the calling conventions.  You might find it useful for
   compile_fdecl.

   [ NOTE: the first six arguments are numbered 0 .. 5 ]
*)

fun arg_loc (n : int) : X86.operand = 
	let
		val _ = if n < 0 then print("Can't find location of a negtive address \n") else ()

		fun find_stack_addr (n : int) : X86.operand =
			let
			in
				Ind3 ((Lit (~8*n)), Rsp)
			end


		val reg = case n of 
					0 => Reg(Rdi)
	  			  | 1 => Reg(Rsi)
				  | 2 => Reg(Rdx)
				  | 3 => Reg(Rcx)
				  | 4 => Reg(R08)
				  | 5 => Reg(R09)
				  | _ => find_stack_addr (n-5)
	in
		reg
	end
	

(* The code for the entry-point of a function must do several things:

   - since our simple compiler maps local %uids to stack slots,
     compiling the control-flow-graph body of an fdecl requires us to
     compute the layout (see the discussion of locals and layout)

   - the function code should also comply with the calling
     conventions, typically by moving arguments out of the parameter
     registers (or stack slots) into local storage space.  For our
     simple compilation strategy, that local storage space should be
     in the stack. (So the function parameters can also be accounted
     for in the layout.)

   - the function entry code should allocate the stack storage needed
     to hold all of the local stack slots.
*)

fun compile_fdecl tdecls (name:ll.gid) ({ fty, param, cfg }:ll.fdecl): X86.prog = (* Hvor skal vi lave epilogue (mov,pop,ret) ?*)
  
  (*layout: 1. Får alle uider ud af cfg. 
              De er i blocken under insns. Du har en liste, hvor du skal tjekke om uid'en eksisterer, 
              hvis det gør skal den addes til resulten. Resulten er en uid liste.*) 
  let
    fun get_uids () : uid list = 
          let 
            fun getinsfromblock (bl:block): uid list = 
                  let
                    val (uids, _) = ListPair.unzip (#insns bl)
                    fun finduid (uidlist:uid option list) : uid list = let
                                                      val headuid = (case (List.hd uidlist) of SOME(u) => [u]
                                                                          | NONE => [])
                                                      val result = (case (List.tl uidlist) of [] => headuid
                                                                                        |_ => headuid @ (finduid (List.tl uidlist)))
                                                     in
                                                      result
                                                     end
                  in
                    finduid uids
                  end
            val uids_fromblock = getinsfromblock (#1 cfg)
            val (_,lblblocks) = ListPair.unzip (#2 cfg)
            val uids_fromlblblock = map getinsfromblock lblblocks
          in
            param @ uids_fromblock @ (List.concat uids_fromlblblock)
          end          

    val alluids = get_uids()
    val imm_loc = (List.length alluids)*8

(*layout: 2. laver lokationen hvor de  skal lægge - dvs den første starter ved rbp-8, 
                og hver ny uid tager -8 i forhold til den tidligere lokation. *)

    fun allocate_locals (loc_uids:ll.uid list) (loc_offset:int) : ((ll.uid * X86.operand) list) =
        let
          val loc_uid = List.hd loc_uids
          val loc_sub = Subq
          val loc_suboper = Ind3 ((Lit loc_offset), Rbp)
          val curr_layout = [(loc_uid,loc_suboper)]
          val result_layout = (case List.tl loc_uids of [] => curr_layout
                                                        |_ => curr_layout @ (allocate_locals (List.tl loc_uids) (loc_offset-8)))
        in
          result_layout
        end

    val loc_layout = allocate_locals alluids (~8)
    
    (*laver prologue*)

    val xpush = Pushq
    val xpushoper = [Reg Rbp]
    val pushins = (xpush,xpushoper)

    val xmov = Movq
    val xmovoper = [Reg Rsp, Reg Rbp]
    val movins = (xmov,xmovoper)

    val xsub = Subq
    val xsuboper = [Imm (Lit imm_loc), Reg Rsp]
    val subins = (xsub,xsuboper)

    val prologue_ins = [pushins,movins,subins]

    val cfgblock = #1 cfg
    val cfg_lblblocks = #2 cfg

    (*moving function arguments from registers to stack*)
    fun movearguments arglist argnum : ins list = 
        let
          val argument = List.hd arglist
          val result = case List.tl arglist of [] => [(Movq, [arg_loc argnum,lookup loc_layout argument])]
                                               |_ => [(Movq, [arg_loc argnum,lookup loc_layout argument])] @ (movearguments (List.tl arglist) (argnum+1))
        in
          result
        end

    val move_instructions = (case param of [] => [] 
                                           |_ => movearguments param 0)

    (*laver en elem liste (prog) ud af al det ovenstående*)
    val ctxt = { tdecls=tdecls, layout=loc_layout }
    val blockins = compile_block ctxt cfgblock
    val prologue_lbl = S.name name
    val prologue_global = true

    val prologue_asm = Text (prologue_ins @ move_instructions @ blockins)
    val functions_elem = {lbl=prologue_lbl, global = prologue_global, asm = prologue_asm}

    val lblblock_elems = map (compile_lbl_block ctxt) cfg_lblblocks
    
  in
    functions_elem :: lblblock_elems
  end


(* compile_gdecl ------------------------------------------------------------ *)

(* Compile a global value into an X86 global data declaration and map
   a global uid to its associated X86 label.
*)

fun compile_ginit GNull        = [Quad (Lit 0)]
  | compile_ginit (GGid gid)   = [Quad (Lbl (mangle gid))]
  | compile_ginit (GInt c)     = [Quad (Lit c)]
  | compile_ginit (GString s)  = [Asciz s]
  | compile_ginit (GArray gs)  = List.concat (List.map compile_gdecl gs)
  | compile_ginit (GStruct gs) = List.concat (List.map compile_gdecl gs)

and compile_gdecl (_, g) = compile_ginit g

(* compile_prog ------------------------------------------------------------- *)

fun compile_prog ({tdecls, gdecls, fdecls}:ll.prog) : X86.prog =
  let fun g (lbl, gdecl) = Asm.data (mangle lbl) (compile_gdecl gdecl)
      fun f (name, fdecl) = compile_fdecl tdecls name fdecl
  in (List.map g gdecls) @ (List.concat (List.map f fdecls))
  end

end (* structure X86Backend *)
