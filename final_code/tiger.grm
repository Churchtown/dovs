structure A = Absyn
structure S = Symbol

(* [these functions and similar ones may be convenient
 * for the creation of abstract syntax trees] *)

datatype lvaluePartSpec = Field of S.symbol
                        | Subscript of A.exp

fun makeLvaluePartSpec (v, pos, l::r) =
  (case l
    of Field idsym =>
       makeLvaluePartSpec (A.FieldVar (v, idsym, pos), pos, r)
     | Subscript exp =>
       makeLvaluePartSpec (A.SubscriptVar (v, exp, pos), pos,r))
  | makeLvaluePartSpec (v, _, nil) = v

fun makeBinop (e1, bop, e2, p) =
    A.OpExp  { left = e1
             , oper = bop
             , right = e2
             , pos = p}

fun makeIf (et, en, el, p) =
    A.IfExp  { test = et
             , thn = en
             , els = el
             , pos = p}

fun makeVarDec (idsym, ty, e, p) =
    A.VarDec { name = idsym
             , escape = ref true
             , typ = ty
             , init = e
             , pos = p}

fun makeFundecl (idsym, ps, rty, e, p) =
             { name = idsym
             , params = ps
             , result = rty
             , body = e
             , pos = p } : A.fundecldata

%%
%term
    EOF
  | ID of string
  | INT of int | STRING of string
  | COMMA | COLON | SEMICOLON | LPAREN | RPAREN | LBRACK | RBRACK
  | LBRACE | RBRACE | DOT
  | PLUS | MINUS | TIMES | DIVIDE | EQ | NEQ | LT | LE | GT | GE | CARET
  | AND | OR | ASSIGN
  | ARRAY | IF | THEN | ELSE | WHILE | FOR | TO | DO | LET | IN | END | OF
  | BREAK | NIL
  | FUNCTION | VAR | TYPE
  | UMINUS | LOW

(* [add missing nonterminals; in second phase: add semantic values] *)

%nonterm program of A.exp | exp of A.exp 
                          | expseq of A.exp
                          | dec of A.decl
                          | decs of A.decl list 
                          | funcall of A.exp 
                          | funarguments of (A.exp * int) list 
                          | arithm of A.exp 
                          | record of A.exp 
                          | recordfields of (Symbol.symbol * A.exp * int) list 
                          | fundec of {name : Symbol.symbol, params : A.fielddata list, result : (Symbol.symbol * pos) option, body : A.exp, pos : pos}
                          | fundecs of {name : Symbol.symbol, params : A.fielddata list, result : (Symbol.symbol * pos) option, body : A.exp, pos : pos}  list
                          | vardec of A.decl
                          | tydec of {name:Symbol.symbol, ty:A.ty, pos:pos}
                          | tydecs of {name:Symbol.symbol, ty:A.ty, pos:pos} list
                          | lvalue of A.var 
                          | ty of A.ty 
                          | tyfields of A.fielddata list 
                          | tyfieldseq of A.fielddata list 
                          | logical of A.exp 
                          | comp of A.exp 
                          | expseqrest of (A.exp * int) list 

%pos int
%verbose
%start program
%eop EOF
%noshift EOF

%name Tiger

%keyword WHILE FOR TO BREAK LET IN END FUNCTION
         VAR TYPE ARRAY IF THEN ELSE DO OF NIL

%prefer THEN ELSE LPAREN

%value ID ("bogus")
%value INT (1)
%value STRING ("")

(* [specify precedence/associativity, least tight binding first] *)
%nonassoc   LOW
%nonassoc   ASSIGN ID
%nonassoc   FUNCTION VAR TYPE THEN DO OF LBRACK RBRACK
%right      ELSE
%left       OR
%left       AND
%nonassoc   EQ NEQ GT LT LE GE
%left       PLUS MINUS
%left       TIMES DIVIDE
%left       UMINUS
%right      CARET
(* [..more tokens & levels..] *)

%%

(* Top level constructs *)

program: exp                  (exp)

lvalue: ID                       (A.SimpleVar (Symbol.symbol ID, IDleft))
      | lvalue DOT ID            (A.FieldVar (lvalue, (Symbol.symbol ID), lvalueleft))
      | lvalue LBRACK exp RBRACK (A.SubscriptVar (lvalue, exp, lvalueleft))
      | ID LBRACK exp RBRACK     (A.SubscriptVar(A.SimpleVar(Symbol.symbol ID, IDleft), exp, IDleft)) (* For some reason, we need this to make it distinguish between array assignment and creation..*)

exp:  (* sequence of expressions *)
     lvalue                   (A.VarExp lvalue)
   | lvalue ASSIGN exp        (A.AssignExp ({var=lvalue,exp=exp,pos=lvalueleft}))
   | NIL                      (A.NilExp)
   | INT                      (A.IntExp INT) (* The payload of INT is accessed this way.. *)
   | STRING                   (A.StringExp (STRING,STRINGleft))
   | arithm                   (arithm) (* arithmetic operations exp OP exp *)
   | LPAREN expseq RPAREN     (expseq)
   | logical                  (logical)
   | comp                     (comp) (* compare expressions *)
   | MINUS exp %prec UMINUS   (makeBinop(A.IntExp 0, A.MinusOp, exp, MINUSleft))
   | funcall                  (funcall) (* a function call on form id() or id(exp1,exp2,exp3...) *)
   | record                   (record) (* a record of form "record_id {id1=ex1,id2=exp2...}" *)
   | ID LBRACK exp RBRACK OF exp (A.ArrayExp ({typ = Symbol.symbol ID, size = exp1, init = exp2, pos = IDleft})) (* array creation *)
   | IF exp THEN exp          (makeIf (exp1, exp2, NONE, IFleft))
   | IF exp THEN exp ELSE exp (makeIf (exp1, exp2, SOME exp3, IFleft))
   | WHILE exp DO exp         (A.WhileExp ({test = exp1, body = exp2, pos = WHILEleft}))
   | FOR ID ASSIGN exp TO exp DO exp (A.ForExp ({var = Symbol.symbol ID, escape = (ref true), lo = exp1, hi = exp2, body = exp3, pos = FORleft})) (* for loop *)
   | BREAK                    (A.BreakExp BREAKleft)
   | LET decs IN expseq END   (A.LetExp ({decls=decs,body=expseq,pos=LETleft}))

comp: exp EQ exp               (makeBinop(exp1,A.EqOp,exp2,exp1left))
    | exp NEQ exp              (makeBinop(exp1,A.NeqOp,exp2,exp1left))
    | exp GT exp               (makeBinop(exp1,A.GtOp,exp2,exp1left))
    | exp LT exp               (makeBinop(exp1,A.LtOp,exp2,exp1left))
    | exp LE exp               (makeBinop(exp1,A.LeOp,exp2,exp1left))
    | exp GE exp               (makeBinop(exp1,A.GeOp,exp2,exp1left))
	
arithm: exp PLUS exp             (makeBinop(exp1,A.PlusOp,exp2,exp1left))
      | exp MINUS exp            (makeBinop(exp1,A.MinusOp,exp2,exp1left))
      | exp TIMES exp            (makeBinop(exp1,A.TimesOp,exp2,exp1left))
      | exp DIVIDE exp           (makeBinop(exp1,A.DivideOp,exp2,exp1left))
      | exp CARET exp            (makeBinop(exp1,A.ExponentOp,exp2,exp1left))

logical: exp AND exp             (A.IfExp ({test = exp1, thn = exp2, els = SOME (A.IntExp 0), pos = exp1left}))
       | exp OR exp              (A.IfExp ({test = exp1, thn = A.IntExp 1, els = SOME exp2, pos = exp1left}))

funcall: ID LPAREN RPAREN                (A.CallExp({func = Symbol.symbol ID, args = [], pos = IDleft}))
       | ID LPAREN funarguments RPAREN   (A.CallExp({func = Symbol.symbol ID, args = funarguments, pos = IDleft}))


fundec: FUNCTION ID LPAREN tyfields RPAREN EQ exp ({name = Symbol.symbol ID, params = tyfields, result = NONE, body = exp, pos = FUNCTIONleft})
      | FUNCTION ID LPAREN tyfields RPAREN COLON ID EQ exp ({name = Symbol.symbol ID1, params = tyfields, result = SOME (Symbol.symbol ID2, ID2left), body = exp, pos = FUNCTIONleft})

fundecs: fundec                     ([fundec])
       | fundecs fundec             (fundecs @ [fundec])

funarguments: exp                     ([(exp,expleft)])
	      | exp COMMA funarguments  ((exp,expleft) :: funarguments)
		  
record: ID LBRACE recordfields RBRACE   (A.RecordExp ({fields = recordfields, typ = Symbol.symbol ID, pos = IDleft}))
		  
recordfields: (* can be empty*)                ([])
	      | ID EQ exp                      ([(Symbol.symbol ID, exp, IDleft)])
	      | COMMA ID EQ exp recordfields   ((Symbol.symbol ID, exp, IDleft) :: recordfields)

expseq: (* can be empty *)            (A.SeqExp [])
      | exp                           (A.SeqExp [(exp, expleft)])
      | SEMICOLON exp expseqrest      (A.SeqExp ((exp, expleft) :: expseqrest))

expseqrest:                           ([])
          | exp                           ([(exp, expleft)])
          | exp SEMICOLON expseqrest      ((exp, expleft) :: expseqrest)

vardec: VAR ID ASSIGN exp             (A.VarDec ({name=Symbol.symbol ID, escape = ref true, typ = NONE, init = exp,pos = VARleft}))
      | VAR ID COLON ID ASSIGN exp    (A.VarDec ({name=Symbol.symbol ID1, escape = ref true, typ = SOME (S.symbol ID2, ID2left), init = exp,pos = VARleft}))


dec: vardec                         (vardec)
   | tydecs    %prec LOW            (A.TypeDec tydecs) 
   | fundecs   %prec LOW            (A.FunctionDec fundecs)

decs: (* can be empty*)       ([])
    | dec                     ([dec])
    | decs dec                (decs @ [dec])
	     
tydec: TYPE ID EQ ty                  ({name=Symbol.symbol ID, ty=ty, pos=TYPEleft})

tydecs: tydec                 ([tydec])
      | tydecs tydec          (tydecs @ [tydec])

ty: ID                                (A.NameTy (Symbol.symbol ID, IDleft))
  | LBRACE tyfields RBRACE            (A.RecordTy tyfields)
  | ARRAY OF ID                       (A.ArrayTy (Symbol.symbol ID, ARRAYleft))

tyfields: (* can be empty *)          ([])
	  | ID COLON ID tyfieldseq      ({name = Symbol.symbol ID1, escape = (ref true), typ = (Symbol.symbol ID2, ID2left), pos = ID1left}::tyfieldseq)

tyfieldseq: (* can be empty *)        ([])
	 | COMMA ID COLON ID tyfieldseq ({name = Symbol.symbol ID1, escape = (ref true), typ = (Symbol.symbol ID2, ID2left), pos = ID1left}::tyfieldseq)
    
    (* [..missing rules for exp..] *)

(* [..missing rules for missing nonterminals..] *)
