(* Aarhus University *)
(* Compilation 2016  *)
(* DO NOT DISTRIBUTE *)

(* DO NOT CHANGE THIS FILE *)


(* X86lite language representation. *)

structure X86 =
struct
(* assembler syntax --------------------------------------------------------- *)

(* Labels for code blocks and global data. *)
type lbl = string

type quad = int

(* Immediate operands *)
datatype imm = Lit of quad
         | Lbl of lbl

(* Registers:
    instruction pointer: rip
    arguments: rdi, rsi, rdx, rcx, r09, r08
    callee-save: rbx, rbp, r12-r15
 *)

datatype reg = Rip
         | Rax | Rbx | Rcx | Rdx | Rsi | Rdi | Rbp | Rsp
         | R08 | R09 | R10 | R11 | R12 | R13 | R14 | R15

datatype operand = Imm of imm        (* immediate *)
             | Reg of reg            (* register *)
             | Ind1 of imm           (* indirect: displacement *)
             | Ind2 of reg           (* indirect: (%reg) *)
             | Ind3 of (imm * reg)   (* indirect: displacement(%reg) *)

(* Condition Codes *)
datatype cnd = Eq | Neq | Gt | Ge | Lt | Le

datatype opcode = Movq | Pushq | Popq
            | Leaq (* Load Effective Address *)
            | Incq | Decq | Negq | Notq
            | Addq | Subq | Imulq | Xorq | Orq | Andq
            | Shlq | Sarq | Shrq
            | Jmp  | J of cnd
            | Cmpq  | Set of cnd
            | Callq | Retq
            | Cqto | Idivq

(* An instruction is an opcode plus its operands.
   Note that arity and other constraints about the operands
   are not checked. *)
type ins = opcode * operand list

datatype data = Asciz of string
              | Quad of imm

datatype asm = Text of ins list    (* code *)
             | Data of data list   (* data *)

(* labeled blocks of data or code *)
type elem = { lbl: lbl, global: bool, asm: asm }

type prog = elem list

(* Provide some syntactic sugar for writing x86 code in SML files. *)
structure Asm =
struct
  fun ~$ i = Imm (Lit i)                     (* int64 constants *)
  fun ~$$ l = Imm (Lbl l)                    (* label constants *)
  fun ~% r = Reg r                           (* registers *)

  (* helper functions for building blocks of data or code *)
  fun data l ds = { lbl = l, global = true, asm = Data ds }
  fun text l is = { lbl = l, global = false, asm = Text is }
  fun gtext l is = { lbl = l, global = true, asm = Text is }
end;

(* pretty printing ----------------------------------------------------------- *)

fun string_of_reg (r:reg) : string =
  case r of
    Rip => "%rip"
  | Rax => "%rax" | Rbx => "%rbx" | Rcx => "%rcx" | Rdx => "%rdx"
  | Rsi => "%rsi" | Rdi => "%rdi" | Rbp => "%rbp" | Rsp => "%rsp"
  | R08 => "%r8"  | R09 => "%r9"  | R10 => "%r10" | R11 => "%r11"
  | R12 => "%r12" | R13 => "%r13" | R14 => "%r14" | R15 => "%r15"

fun string_of_lbl (l:lbl) : string = l

fun IntToString i =
  if i >= 0 then Int.toString i
            else "-" ^ (Int.toString (~i))

fun string_of_imm (Lit i) = IntToString i
  | string_of_imm (Lbl l) = string_of_lbl l

fun string_of_operand (oper:operand) : string =
  case oper of
    Imm i => "$" ^ string_of_imm i
  | Reg r => string_of_reg r
  | Ind1 i => string_of_imm i
  | Ind2 r => "(" ^ string_of_reg r ^ ")"
  | Ind3 (i, r) => string_of_imm i ^ "(" ^ string_of_reg r ^ ")"

fun string_of_jmp_operand (oper:operand) : string =
  case oper of
    Imm i => string_of_imm i
  | Reg r => "*" ^ string_of_reg r
  | Ind1 i => "*" ^ string_of_imm i
  | Ind2 r => "*" ^ "(" ^ string_of_reg r ^ ")"
  | Ind3 (i, r) => "*" ^ string_of_imm i ^ "(" ^ string_of_reg r ^ ")"

fun string_of_cnd (c:cnd) : string =
  case c of
    Eq => "e"  | Neq => "ne" | Gt => "g"
  | Ge => "ge" | Lt => "l"   | Le => "le"

fun string_of_opcode (opc:opcode) : string =
  case opc of
    Movq => "movq" | Pushq => "pushq" | Popq => "popq"
  | Leaq => "leaq"
  | Incq => "incq" | Decq => "decq" | Negq => "negq" | Notq => "notq"
  | Addq => "addq" | Subq => "subq" | Imulq => "imulq"
  | Xorq => "xorq" | Orq => "orq"  | Andq => "andq"
  | Shlq => "shlq" | Sarq => "sarq" | Shrq => "shrq"
  | Jmp  => "jmp"  | J c => "j" ^ string_of_cnd c
  | Cmpq => "cmpq" | Set c => "set" ^ string_of_cnd c
  | Callq => "callq" | Retq => "retq" | Cqto => "cqto" | Idivq => "idivq"

fun map_concat s f l = String.concatWith s (List.map f l)

fun string_of_shift oper args =
  case args of
    args as [Imm i, dst] =>
    "\t" ^ string_of_opcode oper ^ "\t" ^ map_concat ", " string_of_operand args
  | [Reg Rcx, dst] =>
    "\t" ^ string_of_opcode oper ^ ",\t" ^ string_of_operand dst
  | args => raise Fail ("shift instruction has invalid operands: " ^
                         (map_concat ", " string_of_operand args))

fun string_of_ins (oper, args) : string =
  case oper of
    Shlq => string_of_shift oper args
  | Sarq => string_of_shift oper args
  | Shrq => string_of_shift oper args
  | _ =>
    let val f =
      case oper of
        J _ => string_of_jmp_operand
      | Jmp => string_of_jmp_operand
      | Callq => string_of_jmp_operand
      | _ => string_of_operand
  in "\t" ^ string_of_opcode oper ^ "\t" ^ map_concat ", " f args
  end


fun as_encode s =
let
  fun ase [] = []
    | ase (#"\\"::cs) = #"\\" :: #"\\" :: (ase cs)
    | ase (#"\""::cs) = #"\\" :: #"\"" :: (ase cs)
    | ase (c::cs) =
      let
          val ordc = ord(c)
          val charsc = Int.fmt StringCvt.OCT ordc
      in
          if 32 <= ordc andalso ordc < 128 then c :: (ase cs)
          else (#"\\" :: (explode charsc)) @ (ase cs)
      end
in
  (implode o ase o explode) s
end



fun string_of_data (Asciz s) = "\t.asciz\t" ^ "\"" ^ (as_encode s) ^ "\""
  | string_of_data (Quad i) = "\t.quad\t" ^ string_of_imm i


fun string_of_asm (Text is) = "\t.text\n" ^ map_concat "\n" string_of_ins is
  | string_of_asm (Data ds) = "\t.data\n" ^ map_concat "\n" string_of_data ds

fun string_of_elem {lbl, global, asm} : string =
  let val (sec, body) =
        case asm of
          Text is => ("\t.text\n", map_concat "\n" string_of_ins is)
        | Data ds => ("\t.data\n", map_concat "\n" string_of_data ds)
      val glb = if global then "\t.globl\t" ^ string_of_lbl lbl ^ "\n" else ""
  in sec ^ glb ^ string_of_lbl lbl ^ ":\n" ^ body
  end

fun string_of_prog (p:prog) : string =
  String.concatWith "\n" (List.map string_of_elem p)
end
