structure LLEnv = struct

		  
type ll_env = {
    fdecls_rev     : ( ll.gid * ll.fdecl) list ref
  (* reversed list of function declarations *)

  , gdecls_rev     : ( ll.gid * ll.gdecl) list ref
  (* reversed list of global declarations *)

  , tdecls_rev     : ( ll.tid * ll.ty ) list ref
  (* reversed list of type declarations *)


  , temp_names     : int ref
  (* placeholder for generating temporary names *)
			 
}



fun init_ll_env ():ll_env =
  { fdecls_rev     = ref []
  , gdecls_rev     = ref []
  , tdecls_rev     = ref []
  , temp_names     = ref 0

  }


fun mk_name (ll_env:ll_env) (s:string) : Symbol.symbol =
  let val x_ref = #temp_names ll_env
      val x = !x_ref
      val _ = x_ref := x + 1
  in Symbol.symbol (s ^ Int.toString x)
  end

fun mk_uid ll_env s = mk_name ll_env ("U" ^ s)

(* dunno *)
fun mk_gid ll_env s = mk_name ll_env ("G" ^ s)

fun mk_type_name ll_env s =
    mk_name ll_env ("T" ^ s)

fun mk_label_name ll_env s =
    mk_name ll_env ("L" ^ s)

fun add_named_type (ll_env:ll_env) tdecl =
  let val tdecls_rev_ref = #tdecls_rev ll_env
  in tdecls_rev_ref := tdecl :: (!tdecls_rev_ref)
  end
      
fun add_fun_decl (ll_env:ll_env) fdecl =
    let val fdecls_rev_ref = #fdecls_rev ll_env
    in fdecls_rev_ref := fdecl :: (!fdecls_rev_ref)
    end

fun add_gid_decl (ll_env:ll_env) gdecl =
    let val gdecls_rev_ref = #gdecls_rev ll_env
    in  gdecls_rev_ref := gdecl ::  (!gdecls_rev_ref)
    end

fun prog_of_env (env: ll_env) : ll.prog =
  { tdecls = List.rev (! (#tdecls_rev env))
  , gdecls = List.rev (! (#gdecls_rev env))
  , fdecls = List.rev (! (#fdecls_rev env))
  }
	
		  
end
