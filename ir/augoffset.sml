(* AU Compilation 2016.
 *
 * Do not change this file, unless you suspect an error.  Use the
 * course web forum to discuss that.
 *
 * DO NOT DISTRIBUTE
 * 
 *)

(* augoffset.sml 

  Augmentation of the typed abstract trees with the information about
  local variables

*)





signature AUGMENTOFFSET =
sig
  val offsetAugmentProg: TAbsyn.exp -> { exp : OAbsyn.exp, locals: (int * Types.ty) list}
end

structure AugmentOffset: AUGMENTOFFSET =

struct



structure A'  = TAbsyn
structure A'' = OAbsyn

type aug_context = { locals_rev: (int * Types.ty) list ref }

fun init_context() = { locals_rev = ref [] }

fun add_offset (ctxt: aug_context) ty =
  let  val locals_rev  = !(#locals_rev ctxt)
       val n           = List.length locals_rev
       val offset_ty   = (n, ty)
       val _           = (#locals_rev ctxt) := offset_ty::locals_rev 	(* Side effects! *)
  in  (n, ty)
  end

exception AugmentationFatal (* should not happen *)

fun augBinOp oper =
  case oper of
      A'.EqOp   => A''.EqOp
    | A'.NeqOp  => A''.NeqOp
    | A'.LtOp   => A''.LtOp
    | A'.LeOp   => A''.LeOp
    | A'.GtOp   => A''.GtOp
    | A'.GeOp   => A''.GeOp
    | A'.PlusOp => A''.PlusOp
    | A'.MinusOp => A''.MinusOp
    | A'.TimesOp => A''.TimesOp
    | A'.DivideOp => A''.DivideOp
    | A'.ExponentOp => A''.ExponentOp


fun augExp ctxt (typed_exp:TAbsyn.exp): OAbsyn.exp =
  let val augE_ = augExp ctxt
      val exp = #exp typed_exp
      val ty  = (#ty) typed_exp
  in
      case exp
       of A'.NilExp =>
	  { exp = A''.NilExp
	  , aug = { ty = ty }}

	| A'.IntExp i =>
	  { exp = A''.IntExp i
	  , aug = { ty = ty }}

	| A'.StringExp s  =>
	  { exp = A''.StringExp s
	  , aug = { ty = ty }}

	| A'.CallExp {func, args} =>
	  let val args_ = map augE_ args
	  in { exp = A''.CallExp { func = func
				 , args = args_ }
	     , aug = {ty = ty }}
	  end

	| A'.VarExp v =>
	  let val v_ = augVar ctxt v
	  in { exp = A''.VarExp v_
	     , aug = { ty = ty }}
	  end

	| A'.OpExp {left, oper, right}  =>
	  let val left_ = augE_ left
	      val oper_ = augBinOp oper
	      val right_  = augE_ right
	  in { exp = A''.OpExp {left = left_
			       ,oper = oper_
			       ,right= right_}
	     , aug = {ty = ty }}
	  end

	| A'.RecordExp { fields } =>
	  let val fields_ = map (fn (s, e) => (s, augE_ e)) fields
	  in { exp = A''.RecordExp { fields = fields_ }
	     , aug = {ty = ty }}
	  end

	| A'.SeqExp exps =>
	  let val exps_ = map augE_ exps
	  in { exp = A''.SeqExp exps_
	     , aug = { ty = ty }}
	  end

	| A'.AssignExp { var, exp }  =>
	  let val var_ = augVar ctxt var
	      val exp_ = augE_ exp
	  in { exp = A''.AssignExp { var = var_
				   , exp = exp_ }
	     , aug = {ty = ty }}
	  end

	| A'.IfExp {test, thn, els}  =>
	  let val test_ = augE_ test
	      val thn_  = augE_ thn
	      val els_  = case els of
			      SOME els' => SOME (augE_ els')
			    | NONE => NONE
	  in { exp = A''.IfExp { test = test_
			       , thn  = thn_
			       , els  = els_ }
	     , aug = { ty = ty }}
	  end

	| A'.WhileExp {test, body} =>
	  let val test_ = augE_ test
	      val body_ = augE_ body
	  in { exp = A''.WhileExp { test = test_
				  , body = body_ }
	     , aug = { ty = ty }}
	  end

	| A'.ForExp { var, escape, lo, hi, body } =>
	  let val (n,_) = add_offset ctxt Types.INT (* allocate space for the loop index *)
	      val lo_ = augE_ lo
	      val hi_ = augE_ hi
	      val body_ = augE_ body

	  in  { exp = A''.ForExp { var = var
				 , escape = escape
				 , lo = lo_
				 , hi = hi_
				 , body = body_
				 , aug = { ty = ty, offset = n }}
	      , aug = { ty = ty }}
	  end

	| A'.BreakExp => { exp = A''.BreakExp, aug = { ty = ty }}

	| A'.LetExp {decls, body} =>
	  let val decls_ = augDecs ctxt decls
	      val body_ = augExp ctxt body
	  in { exp = A''.LetExp { decls = decls_
				, body  = body_ }
	     , aug = { ty = ty }}
	  end

	| A'.ArrayExp { size, init } =>
	  let val size_ = augE_ size
	      val init_ = augE_ init
	  in { exp = A''.ArrayExp { size = size_
				  , init = init_ }
	     , aug = { ty = ty }}
	  end

	| A'.ErrorExp => { exp = A''.ErrorExp
			 , aug = { ty = ty }}

  end

(* augmentation of variable usage *)
and augVar ctxt  (v as {var, ty } ) =
    case var
     of	A'.SimpleVar s =>
	{ var = A''.SimpleVar s
	, aug = { ty = ty }}

      | A'.FieldVar (var, s) =>
	{ var = A''.FieldVar ( augVar ctxt var, s)
	, aug = { ty = ty }}

      | A'.SubscriptVar (var, exp) =>
	let val var_ = augVar ctxt var
	    val exp_ = augExp ctxt exp
	in { var = A''.SubscriptVar (var_, exp_)
	   , aug = { ty = ty }}
	end


(* augmentation of declarations *)
and augDecs (ctxt:aug_context) ds =
    let	fun augDec dec =
	  case dec
	   of A'.FunctionDec fns =>
	      let fun augFn ({name, params, resultTy, body} ) :A''.fundecldata =
		    let val fn_ctxt = init_context()
			fun augArg {name, escape, ty} =
			  (* this is similar to variable declaration augmentation *)
			  let val (n, _ ) = add_offset fn_ctxt ty
			  in { name = name
			     , escape = escape
			     , aug = { ty = ty, offset = n }}
			  end

			val args_  = map augArg params
			val body_  = augExp fn_ctxt body
			val locals = List.rev (!(#locals_rev fn_ctxt))
		    in
			{ name = name
			, args = args_
			, aug  = { ty = resultTy, locals = locals }
			, body = body_
			}
		    end
	      in A''.FunctionDec (map augFn fns)
	      end

	    | A'.TypeDec ls => A''.TypeDec ls

	    | A'.VarDec {name, escape, ty, init} =>
	      let val init_       = augExp ctxt init
		  val (n,_ )      = add_offset ctxt ty

	      in A''.VarDec { name = name
			    , escape = escape
			    , aug = {ty = ty, offset = n}
			    , init = init_ }
	      end
    in map augDec ds
    end


fun offsetAugmentProg e =
   let val ctxt = init_context ()
       val e' = augExp ctxt e
   in { exp = e', locals = List.rev (!(#locals_rev ctxt))}
   end

end
