(* -*- mode:sml -*- *)

type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult = (svalue, pos) token
val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos
val commentNum = ref 0
val stringLineNum = ref 0
val unclosedString = ref false
val stringSoFar = ref ""
val beginningEscapeLine = ref 0
exception ConversionException

fun err (p1,p2) = ErrorMsg.error p1

fun eof () =
      let
      val pos = hd (!linePos)
      in
  (if !commentNum <> 0 then (ErrorMsg.error pos "Unclosed comment in code!") else
		       if !unclosedString then (ErrorMsg.error pos ("Unclosed string in code at line " ^ (Int.toString (!stringLineNum)) ^ "!"))
			else (print "No unclosed comments or strings \n");
   Tokens.EOF(pos,pos))
      end

fun s2i t pos =
    let
        val opti = (Int.fromString t) 
            handle Overflow => 
                   (ErrorMsg.error pos "Integer too large"; SOME 0)
        fun s2i_aux NONE = (ErrorMsg.error pos "Ill-formed integer"; 0)
          | s2i_aux (SOME n) = n
    in
        s2i_aux opti
    end

fun dopos token yypos yylen = token (yypos, yypos + yylen)
fun dopos3 token value yypos yylen = token (value, yypos, yypos + yylen)
fun convertControlChar yypos value lineNumber =
  let val result = String.fromString value
      in
         case result of SOME(n) => n
  | NONE => (ErrorMsg.error yypos ("Cannot convert controlchar at line " ^ (Int.toString lineNumber)); "")
  end

%%

%header (functor TigerLexFun (structure Tokens: Tiger_TOKENS));

%s STRING COMMENT ESCAPE;
letter=[a-zA-Z];
digits=[0-9]+;
idchars=[a-zA-Z]+[a-zA-Z0-9_]*;
controlchars=\^[A-Z@\[\]\\\^_?];
newline=\n;
tab=\\t;
quote = [\"];
escapechars=(n|t|[0-9]{3}|{quote}|\\);
%%
<INITIAL> "if"                     => (dopos Tokens.IF yypos 2); 
<INITIAL> "array"                  => (dopos Tokens.ARRAY yypos 5);
<INITIAL> ":="                     => (dopos Tokens.ASSIGN yypos 2);
<INITIAL> "|"                      => (dopos Tokens.OR yypos 1);               
<INITIAL> "&"                      => (dopos Tokens.AND yypos 1);
<INITIAL> ">="                     => (dopos Tokens.GE yypos 2);
<INITIAL> ">"                      => (dopos Tokens.GT yypos 1);
<INITIAL> "<="                     => (dopos Tokens.LE yypos 2);
<INITIAL> "<"                      => (dopos Tokens.LT yypos 1);
<INITIAL> "<>"                     => (dopos Tokens.NEQ yypos 2);
<INITIAL> "="                      => (dopos Tokens.EQ yypos 1);
<INITIAL> "/"                      => (dopos Tokens.DIVIDE yypos 1);
<INITIAL> "*"                      => (dopos Tokens.TIMES yypos 1);
<INITIAL> "-"                      => (dopos Tokens.MINUS yypos 1);
<INITIAL> "+"                      => (dopos Tokens.PLUS yypos 1);
<INITIAL> {newline}	               => (lineNum := !lineNum+1; continue());
<INITIAL> " "                      => (continue());
<INITIAL> "."                      => (dopos Tokens.DOT yypos 1);
<INITIAL> "}"                      => (dopos Tokens.RBRACE yypos 1);
<INITIAL> "{"                      => (dopos Tokens.LBRACE yypos 1);
<INITIAL> "]"                      => (dopos Tokens.RBRACK yypos 1);
<INITIAL> "["                      => (dopos Tokens.LBRACK yypos 1);
<INITIAL> "("                      => (dopos Tokens.LPAREN yypos 1);
<INITIAL> ")"                      => (dopos Tokens.RPAREN yypos 1);
<INITIAL> ";"                      => (dopos Tokens.SEMICOLON yypos 1);
<INITIAL> ":"                      => (dopos Tokens.COLON yypos 1);
<INITIAL> ","                      => (dopos Tokens.COMMA yypos 1);
<INITIAL> "^"                      => (dopos Tokens.CARET yypos 1);
<INITIAL> {quote}                  => (YYBEGIN STRING; unclosedString := true; stringLineNum := !lineNum; stringSoFar := ""; continue());
<INITIAL>  ","                     => (dopos Tokens.COMMA yypos 1);
<INITIAL> "var"                    => (dopos Tokens.VAR yypos 3);
<INITIAL> "type"                   => (dopos Tokens.TYPE yypos 4);
<INITIAL> "function"               => (dopos Tokens.FUNCTION yypos 8);
<INITIAL> "break"                  => (dopos Tokens.BREAK yypos 5);
<INITIAL> "of"                     => (dopos Tokens.OF yypos 2);
<INITIAL> "end"                    => (dopos Tokens.END yypos 3);
<INITIAL> "in"                     => (dopos Tokens.IN yypos 2);
<INITIAL> "nil"                    => (dopos Tokens.NIL yypos 3);
<INITIAL> "let"                    => (dopos Tokens.LET yypos 3);
<INITIAL> "do"                     => (dopos Tokens.DO yypos 2);
<INITIAL> "to"                     => (dopos Tokens.TO yypos 2);
<INITIAL> "for"                    => (dopos Tokens.FOR yypos 3);
<INITIAL> "while"                  => (dopos Tokens.WHILE yypos 5);
<INITIAL> "else"                   => (dopos Tokens.ELSE yypos 4);
<INITIAL, COMMENT> [\ \n\t]        => (continue());
<INITIAL> {digits}                 => (dopos3 Tokens.INT (s2i yytext yypos) yypos (size yytext));
<INITIAL> {idchars}                => (dopos3 Tokens.ID yytext yypos (size yytext));
<INITIAL>  "/*"                    => (commentNum := !commentNum+1; YYBEGIN COMMENT; continue());
<COMMENT> "/*"                     => (commentNum := !commentNum+1; continue());
<COMMENT> "*/"                     => (commentNum := !commentNum-1; if !commentNum = 0 then (YYBEGIN INITIAL; continue()) else continue());
<COMMENT> .                        => (continue());
<STRING> [^\"\\\n]*                 => (stringSoFar := (!stringSoFar ^ yytext); continue());
<STRING> "\\"                      => (YYBEGIN ESCAPE; beginningEscapeLine := !lineNum; continue());
<STRING> {quote}                   => (YYBEGIN INITIAL; unclosedString := false; dopos3 Tokens.STRING (!stringSoFar) yypos (size (!stringSoFar)));
<STRING> "\n"                      => (ErrorMsg.error yypos ("illegal newline in string at line " ^ (Int.toString (!lineNum))); continue());
<ESCAPE> {escapechars}             => (YYBEGIN STRING; stringSoFar := (!stringSoFar ^ "\\" ^ yytext); continue());
<ESCAPE> {controlchars}            => (YYBEGIN STRING; stringSoFar := (!stringSoFar ^ (convertControlChar yypos ("\\\\" ^ yytext) (!lineNum))); continue());
<ESCAPE> ("\n"|"\t"|" "|"\f")+"\\"     => (stringSoFar := (!stringSoFar ^ yytext); YYBEGIN STRING; continue());
<ESCAPE> .                         => (unclosedString := false; YYBEGIN STRING; ErrorMsg.error yypos ("Illegal character \\ or controlchar at line " ^ (Int.toString (!beginningEscapeLine))); continue());   
<INITIAL> .                        => (ErrorMsg.error yypos ("illegal character " ^ yytext ^ " at line " ^ (Int.toString (!lineNum))); continue());
