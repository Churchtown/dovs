(* ---------- Initial code for hand-in 1, dOvs 2018: warmup.sml ---------- *)

(* Before you start working:

   1. Read the documentation on the course webpage AND the description
      of the assignment in the textbook.

   2. Read this file until the end.

   If there are things that are unclear about this file, consider
   posting your questions on the webboard; it is likely that others
   may have the same concerns.                                             *)

structure SLgrammar = struct

type id = string

datatype binop = Plus | Minus | Times | Div

datatype stm = CompoundStm of stm * stm
             | AssignStm of id * exp
             | PrintStm of exp list
     and exp = IdExp of id
             | NumExp of int
             | OpExp of exp * binop * exp
             | EseqExp of stm * exp
                                    
end (* SLgrammar *)

(* ----- Fill in: Interpret SLgrammar as specified ----------------------- *)

structure G = SLgrammar

exception NotImplemented (* when the entire assignment is complete, the
                            code should compile with this exception
                            declaration removed                            *)

(* ----------------------------------------------------------------------- *)
(* -- placeholder declarations for not implemented functions ------------- *)

              
(* -- Function stringOfStm: ---------------------------------------------- *)
(* Input: a value of type G.stm that corresponds to a statement in our
   language.
   
   Output: a string representation of that statement.
   
   Note: the assginment does not specify _how_ the string is
   formatted. There may be in general several ways of doing this. The
   example below is just one possibility.

   Example: For example program 'prog' (defined below) a possible output
   is string: "a := 5+3; b := (print(a,a-1), 10*a); print(b)"              *)

fun max(a : int) (b : int) : int =
	if a > b then a else b;
	      
fun maxargs_stm(stm : G.stm) : int =
    case stm of
	G.CompoundStm(x,y) => max (maxargs_stm x) (maxargs_stm y) 
      | G.AssignStm(_,y) => maxargs_exp y
      | G.PrintStm([]) => 0
      | G.PrintStm(head::tail) => max ((length tail)+1) (max (maxargs_stm(G.PrintStm(tail))) (maxargs_exp head))

and maxargs_exp (exp : G.exp) : int =
    case exp of
	G.IdExp(_) => 0 
      | G.NumExp(_) => 0 
      | G.OpExp(x,_,y) => max (maxargs_exp x) (maxargs_exp y) 
      | G.EseqExp(x,y) => max (maxargs_stm x) (maxargs_exp y); 

fun binToString (binop : G.binop) : string =
    case binop of
        G.Plus => " + "
        | G.Minus => " - "
        | G.Times => " * "
        | G.Div => " / ";

fun stringOfStm ( statement : G.stm) : string  =
    case statement of
        G.CompoundStm (x, y) => stringOfStm x ^ stringOfStm y
        | G.AssignStm (x, y) => x ^ " := " ^ expToString y ^ "; "
        | G.PrintStm (head::[]) => "print(" ^ expToString head ^ ")"
        | G.PrintStm (head :: tail) => "print(" ^ expToString head ^ ", " ^ listToString tail ^ ")"

and expToString ( exp : G.exp) : string =
    case exp of
        G.IdExp (x) => x
        | G.NumExp (x) => Int.toString(x)
        | G.OpExp (x, y, z) => expToString x ^ binToString y ^ expToString z
        | G.EseqExp (x, y) => "(" ^ stringOfStm x ^ expToString y ^ ")"

and listToString (tail : G.exp list) : string = 
    case tail of
        [] => "" 
        | head::[] => expToString(head) 
        | _ => expToString (hd tail) ^ ", " ^ listToString (tl tail);


                      
(* -- Function buildEnv -------------------------------------------------- *)

(* Function buildEnv builds an initial environment if the interpreter uses
   a compilation phase.  The assignment page provides more detailed
   information about buildEnv.                                             *)

(* fn : int * (int * int) list -> bool -- Stolen from the internet :))))) *)
fun contains (id : G.id) (tupleList : (G.id * int) list ) =
    if null tupleList
    then false
    else if #1 (hd tupleList) = id
    then true
    else contains id (tl tupleList);

fun assignId (id : G.id) (tupleList : (G.id * int) list) : ((G.id * int) list ) =
    if (contains id tupleList) then
        tupleList
    else 
        (id, 0) :: tupleList;

fun buildEnv (stm : G.stm) : ((G.id * int) list) =
    let val a = [] in 
        handleStm stm a
        end

and handleStm (stm : G.stm) (tupleList : (G.id * int) list) : ((G.id * int) list) =
    case stm of 
        G.CompoundStm (x,y) => handleStm y (handleStm x tupleList) 
        | G.AssignStm (x,y) => assignId x (handleExp y tupleList)
        | G.PrintStm (x) => handleExpList x tupleList

and handleExp (exp : G.exp) (tupleList : ((G.id * int) list)) : ((G.id * int) list) = 
    case exp of 
        G.OpExp (x, _ ,z) => handleExp z (handleExp x tupleList)
        | G.EseqExp (x,y) => handleExp y (handleStm x tupleList)
        | _ => tupleList

and handleExpList (expList : G.exp list) (tupleList : (G.id * int) list) : ((G.id * int) list) = 
    case expList of 
    [] => tupleList 
    | _ => handleExpList (tl expList) (handleExp (hd expList) tupleList);


(* -- Function interpStm ------------------------------------------------- *)          
(* Funtion interpStm interprets a G.stm - Implementation hints can be
   found in the book                                                       *)

exception divisionBy0Error;
          
fun handleBinop (x : int)(operator : G.binop)(y : int): int =
    case operator of
        G.Plus => x + y
        | G.Minus => x - y
        | G.Times => x * y
        | G.Div => (if y = 0 then raise divisionBy0Error else Real.round((Real.fromInt x) / (Real.fromInt y)));

fun searchId (idvalue : (G.id * int))(prev : (G.id * int)list)(current : (G.id * int))(rest : (G.id * int)list) : (G.id * int)list =
    if #1 idvalue = #1 current then
        (prev @ [idvalue] @ rest) else
        searchId idvalue (prev @ [current]) (hd rest) (tl rest);

fun handleId (id : G.id) (tuple : (int * (G.id * int)list)) : (G.id * int)list =
    searchId (id, #1 tuple) [] (hd (#2 tuple)) (tl (#2 tuple));

fun lookUp (id : G.id) (env : (G.id * int) list) : int =
    if (#1 (hd env)) = id then
    (#2 (hd env)) else
    lookUp id (tl env);

fun printIntList (intList : int list) =
    case intList of
	[] => print "" 
      | (head::tail) => (print ((Int.toString head) ^ "\n"); printIntList tail)

fun interpStm (stm : G.stm) (env : (G.id * int) list) : (G.id * int) list =
    case stm of 
        G.CompoundStm (x,y) => interpStm y (interpStm x env)
        | G.AssignStm (x,y) => handleId x (interpExp y env)
        | G.PrintStm (expList) => let val (intList, newEnv) = printRecursiveList expList env
				  in
				      (printIntList intList; newEnv)
				  end
(* This function builds a list of ints that 'printIntList' uses to do the actual printing *)
(* We iterate through the list of expression from left to right, top to bottom, the actual printing is done at the end of all evaluations *)
and printRecursiveList (head::tail : G.exp list) (env : (G.id * int) list) : (int list * (G.id * int) list) =
    case tail of
	[] => let val (result, newEnv) = interpExp head env
	      in
		  ([result],newEnv)
	      end
      | _ => 
	let val (resultOfHead, envOfHead) = interpExp head env
	    val (resultOfTail, envOfTail) = printRecursiveList tail envOfHead
	in
	    (resultOfHead::resultOfTail, envOfTail)
	end
	   
and interpExp (exp : G.exp) (env : (G.id * int) list) : (int * ((G.id * int) list)) = 
    case exp of 
        G.IdExp (x) => ((lookUp x env), env)
        | G.NumExp (x) => (x, env) 
        | G.OpExp (x,y,z) => ((handleBinop (#1(interpExp x env)) y (#1(interpExp z (#2(interpExp x env))))), (#2(interpExp z (#2(interpExp x env)))))
        | G.EseqExp (x,y) => interpExp y (interpStm x env);


(* -- Function printEnv -------------------------------------------------- *)

(* Given an environment (id * int) list, this function prints a string
   representation of the environment.

   Example: Given the environment [("a", 3), ("b", 5)], this function
   prints "[(a, 3), (b, 5)]".

   Note again, there are several ways to format an environment; feel free
   to pick one.                                                            *)

fun printRec (tupleList : (G.id * int) list) = 
    case tupleList of 
    [] => print ""
    | head::[] => (print "("; print (#1(head)); print", "; print (Int.toString (#2(head))); print ")] \n")
    | _ => (print "("; print (#1(hd tupleList)); print", "; print (Int.toString (#2(hd tupleList))); print "), "; printRec (tl tupleList));
    

fun printEnv (tupleList : (G.id * int) list) =
    (print "[";
    printRec tupleList);
    


(* ----------------------------------------------------------------------- *)

fun interp (s: G.stm): unit =
    let val _ = print ("Executing: " ^ (stringOfStm s) ^ "\n")
        val env = buildEnv s
        val env' = interpStm s env
    in printEnv env'
    end

(* -- Example for testing ------------------------------------------------ *)

val prog =
  (* a := 5+3; b := (print(a,a-1), 10*a); print(b) *)
  G.CompoundStm (
    G.AssignStm ("a", G.OpExp (G.NumExp 5, G.Plus, G.NumExp 3)),
    G.CompoundStm (
      G.AssignStm ("b", G.EseqExp (
        G.PrintStm [G.IdExp "a", G.OpExp (G.IdExp "a", G.Minus, G.NumExp 1)],
        G.OpExp (G.NumExp 10, G.Times, G.IdExp "a"))),
      G.PrintStm [G.IdExp "b"]))

val prog2 = 
    (* s := 5/2; t := (2 + 10 * s); n := (print(s, 3*t), s+t) print (t-2*s) *)
    G.CompoundStm (
        G.AssignStm( "s", G.OpExp(G.NumExp 5, G.Div, G.NumExp 2)),
        G.CompoundStm(
        G.AssignStm("t", G.OpExp(G.NumExp 2, G.Plus, (G.OpExp(G.NumExp 10, G.Times, G.IdExp "s")))),
        G.CompoundStm(
            G.AssignStm("n", G.EseqExp (
                G.PrintStm [G.IdExp "s", G.OpExp(G.NumExp 3, G.Times, G.IdExp "t")],
                G.OpExp (G.IdExp "s", G.Plus, G.IdExp "t"))),
            G.PrintStm [G.OpExp (G.IdExp "t", G.Minus, G.OpExp(G.NumExp 2, G.Times, G.IdExp "s"))])))


val prog3 = 
    (* a := 15; b := 12; a2 := a; print(a, b, a2) *)
    G.CompoundStm (
	    G.CompoundStm (
	        G.AssignStm("a", G.NumExp 15), G.AssignStm("b", G.NumExp 12)),
	    G.CompoundStm(
            G.AssignStm("a2", G.IdExp "a"), 
            G.PrintStm [G.IdExp "a", G.IdExp "b", G.IdExp "a2"]))


val prog4 = 
    (* a:= 4; b:= 42; z:= 10; print(a,b,z/a); *)
    G.CompoundStm (
        G.AssignStm("a", G.NumExp 4),
            G.CompoundStm (
                G.AssignStm("b", G.NumExp 42),
                G.CompoundStm(
                    G.AssignStm("z", G.NumExp 10),
                    G.PrintStm([G.IdExp "a", G.IdExp "b", G.OpExp(G.IdExp "z", G.Div, G.IdExp "a")]))))

val prog5 = 
    (* b:= (4 + 4) * 2; f:= 10-2; print(b,f) *)
    G.CompoundStm(
        G.AssignStm("b", G.OpExp((G.OpExp(G.NumExp 4, G.Plus, G.NumExp 4), G.Times, G.NumExp 2))),
        G.CompoundStm(
            G.AssignStm("f", G.OpExp(G.NumExp 10, G.Minus, G.NumExp 2)),
            G.PrintStm([G.IdExp "b", G.IdExp "f"])))

val prog6 = 
    (* print((a := (k := 19 / 3; (h := 5; 3)); h + a) *)
    G.PrintStm [G.EseqExp(
                    G.AssignStm("a", 
                        G.EseqExp(
                            G.AssignStm("k", G.OpExp(G.NumExp 19, G.Div, G.NumExp 3)), 
                        G.EseqExp(
                            G.AssignStm("h", G.NumExp 5),
                            G.NumExp 3))), 
		    G.OpExp(G.IdExp "h", G.Plus, G.IdExp "a"))]

val prog7 = 
    (* Call-by-value: 4, 5, 1, 2, 3, 6, 7, 8 *)
    G.PrintStm([    G.NumExp 1, 
                    G.NumExp 2, 
                    G.NumExp 3, 
                    G.EseqExp(
                        G.PrintStm([G.NumExp 4, G.NumExp 5]), 
                        G.NumExp 6), 
                    G.NumExp 7, 
                    G.NumExp 8])
			  
val prog8 = 
    (* a := 0, b:= 2/a Expected output: error because of division with 0 *)
    G.CompoundStm(
        G.AssignStm("a", G.NumExp 0),
        G.AssignStm("b",
            G.OpExp(G.NumExp 2, G.Div, G.IdExp "a")))

(* Calling the interpreter on the example program.  

   TODO: Uncomment to proceed. Default implementation raises
   NotImplemented exception                                                *)

(* val _ = interp prog *)
                
